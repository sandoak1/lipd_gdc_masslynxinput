using Novartis.Ph.Nibr.Pla.Common;
using Novartis.Ph.Nibr.Pla.WPFUtilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using IntelliScanToMasslynx.WPF.Models;
using System.Collections.ObjectModel;

namespace IntelliScanToMasslynx.WPF
{
    public class DataModel : WPFDataModel
    {
        private static DataModel instance = new DataModel();

        public static DataModel Instance
        {
            get
            {
                return instance;
            }
        }

        private DataModel()
        {
        }

        private bool _isDone = false;
        private bool _isStartButtonEnabled = false;
        private bool _isNotRunning = true;
        private string _intelliScanFilePath;
        private string _configurationFilePath;
        private string _phyChemFilePath;
        private string _userSelectedOutputFolderPath;
        private string _createdOutputFolderName;
        private uint _massLynxSamplesResultFile_FirstSampleInTheWorkList;
        private string _outputFolderPath;
        private ConfigurationFileModel _configuration;
        private ConfigurationFileModel _configurationEdited;
        private bool _isConfigurationLoaded;
        private bool _iIsConfigurationEditedByTheUser;
        private uint _rRunCountAfterUserEditedTheConfiguration;
        private string _userName;
        private ObservableCollection<PlateNumberItem> _plateNumbers = new ObservableCollection<PlateNumberItem>();
        private PlateNumberItem _selectedPlateNumber;

        public string AssemblyProduct
        {
            get
            {
                return Utilities.Instance.AssemblyProduct;
            }
        }

        public bool IsStartButtonEnabled
        {
            get
            {
                return this._isStartButtonEnabled;
            }

            set
            {
                if (this._isStartButtonEnabled != value)
                {
                    this._isStartButtonEnabled = value;
                    base.OnPropertyChanged(nameof(this.IsStartButtonEnabled));
                }
            }
        }

        public bool IsDone
        {
            get
            {
                return this._isDone;
            }

            set
            {
                if (this._isDone != value)
                {
                    this._isDone = value;
                    base.OnPropertyChanged(nameof(this.IsDone));
                }
            }
        }

        public bool IsNotRunning
        {
            get
            {
                return this._isNotRunning;
            }

            set
            {
                if (this._isNotRunning != value)
                {
                    this._isNotRunning = value;
                    base.OnPropertyChanged(nameof(this.IsNotRunning));
                    base.OnPropertyChanged(nameof(this.Running));
                }
            }
        }

        public bool Running
        {
            get
            {
                return !this._isNotRunning;
            }
        }


        public string IntelliScanFilePath
        {
            get
            {
                return this._intelliScanFilePath;
            }

            set
            {
                if (this._intelliScanFilePath != value)
                {
                    this._intelliScanFilePath = value;
                    base.OnPropertyChanged(nameof(this.IntelliScanFilePath));
                }
            }
        }
        
        public string ConfigurationFilePath
        {
            get
            {
                return this._configurationFilePath;
            }

            set
            {
                if (this._configurationFilePath != value)
                {
                    this._configurationFilePath = value;
                    base.OnPropertyChanged(nameof(this.ConfigurationFilePath));
                }
            }
        }

        public string PhyChemFilePath
        {
            get
            {
                return this._phyChemFilePath;
            }

            set
            {
                if (this._phyChemFilePath != value)
                {
                    this._phyChemFilePath = value;
                    base.OnPropertyChanged(nameof(this.PhyChemFilePath));
                }
            }
        }

        public string UserSelectedOutputFolderPath
        {
            get
            {
                return this._userSelectedOutputFolderPath;
            }

            set
            {
                if (this._userSelectedOutputFolderPath != value)
                {
                    this._userSelectedOutputFolderPath = value;
                    base.OnPropertyChanged(nameof(this.UserSelectedOutputFolderPath));
                }
            }
        }

        public string CreatedOutputFolderName
        {
            get
            {
                return this._createdOutputFolderName;
            }

            set
            {
                if (this._createdOutputFolderName != value)
                {
                    this._createdOutputFolderName = value;
                    base.OnPropertyChanged(nameof(this.CreatedOutputFolderName));
                }
            }
        }

        public string OutputFolderPath
        {
            get
            {
                return this._outputFolderPath;
            }

            set
            {
                if (this._outputFolderPath != value)
                {
                    this._outputFolderPath = value;
                    base.OnPropertyChanged(nameof(this.OutputFolderPath));
                }
            }
        }

        public uint MassLynxSamplesResultFile_FirstSampleInTheWorkList
        {
            get
            {
                return this._massLynxSamplesResultFile_FirstSampleInTheWorkList;
            }

            set
            {
                if (this._massLynxSamplesResultFile_FirstSampleInTheWorkList != value)
                {
                    this._massLynxSamplesResultFile_FirstSampleInTheWorkList = value;
                    base.OnPropertyChanged(nameof(this.MassLynxSamplesResultFile_FirstSampleInTheWorkList));
                }
            }
        }

        public ConfigurationFileModel Configuration
        {
            get
            {
                return this._configuration;
            }

            set
            {
                if (this._configuration != value)
                {
                    this._configuration = value;
                    base.OnPropertyChanged(nameof(this.Configuration));
                }
            }
        }

        public ConfigurationFileModel ConfigurationEdited
        {
            get
            {
                return this._configurationEdited;
            }

            set
            {
                if (this._configurationEdited != value)
                {
                    this._configurationEdited = value;
                    base.OnPropertyChanged(nameof(this.ConfigurationEdited));
                }
            }
        }

        public bool IsConfigurationLoaded
        {
            get
            {
                return this._isConfigurationLoaded;
            }

            set
            {
                if (this._isConfigurationLoaded != value)
                {
                    this._isConfigurationLoaded = value;
                    base.OnPropertyChanged(nameof(this.IsConfigurationLoaded));
                }
            }
        }

        public bool IsConfigurationEditedByTheUser
        {
            get
            {
                return this._iIsConfigurationEditedByTheUser;
            }

            set
            {
                if (this._iIsConfigurationEditedByTheUser != value)
                {
                    this._iIsConfigurationEditedByTheUser = value;
                    base.OnPropertyChanged(nameof(this.IsConfigurationEditedByTheUser));
                }
            }
        }

        public uint RunCountAfterUserEditedTheConfiguration
        {
            get
            {
                return this._rRunCountAfterUserEditedTheConfiguration;
            }

            set
            {
                if (this._rRunCountAfterUserEditedTheConfiguration != value)
                {
                    this._rRunCountAfterUserEditedTheConfiguration = value;
                    base.OnPropertyChanged(nameof(this.RunCountAfterUserEditedTheConfiguration));
                }
            }
        }

        public string UserName
        {
            get
            {
                return this._userName;
            }

            set
            {
                if (this._userName != value)
                {
                    this._userName = String.IsNullOrWhiteSpace(value) ? value : value.ToUpper();
                    base.OnPropertyChanged(nameof(this.UserName));
                }
            }
        }

        public ObservableCollection<PlateNumberItem> PlateNumbers
        {
            get
            {
                return this._plateNumbers;
            }

            set
            {
                if (this._plateNumbers != value)
                {
                    this._plateNumbers = value;
                    base.OnPropertyChanged(nameof(this.PlateNumbers));
                }
            }
        }

        public PlateNumberItem SelectedPlateNumber
        {
            get
            {
                return this._selectedPlateNumber;
            }

            set
            {
                if (this._selectedPlateNumber != value)
                {
                    this._selectedPlateNumber = value;
                    base.OnPropertyChanged(nameof(this.SelectedPlateNumber));
                }
            }
        }
    }
}
