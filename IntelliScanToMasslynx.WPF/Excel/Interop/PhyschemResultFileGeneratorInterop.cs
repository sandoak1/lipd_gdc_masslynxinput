﻿using ExcelInterop = Microsoft.Office.Interop.Excel;
using IntelliScanToMasslynx.WPF.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Novartis.Ph.Nibr.Pla.Common;
using System.Diagnostics;
using IntelliScanToMasslynx.WPF.Models;
using System.Reflection;

namespace IntelliScanToMasslynx.WPF.Excel.Interop
{
    public class PhyschemResultFileGeneratorInterop : IDisposable
    {
        private string _path;
        private IList<IntelliScanFileModel> _intelliScanFileData;
        private IList<IntelliScanFileModel> _intelliScanFileDataOrdered;

        public PhyschemResultFileGeneratorInterop(string path, IList<IntelliScanFileModel> intelliScanFileData, IList<IntelliScanFileModel> intelliScanFileDataOrdered)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (intelliScanFileData?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileData));
            }
            if (intelliScanFileDataOrdered?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileDataOrdered));
            }

            this._path = path;
            this._intelliScanFileData = intelliScanFileData;
            this._intelliScanFileDataOrdered = intelliScanFileDataOrdered;
        }

        public void Genereaete()
        {
            ExcelInterop.Application application = new ExcelInterop.Application();
            ExcelInterop.Window window = application.ActiveWindow;
            ExcelInterop.Workbooks workbooks = application.Workbooks;
            ExcelInterop.Workbook workbook = workbooks.Open(this._path);
            ExcelInterop.Sheets workbooksheets = workbook.Worksheets;
            ExcelInterop.Worksheet finalPatientSummariesSheet = workbooksheets[Constants.ResultsforchemistsSheetName];
            application.DisplayAlerts = false;
            application.ScreenUpdating = false;
            application.Visible = false;

            int insertedRowCount = 0;
            string prevoiusUserName = null;
            var orderedData = this._intelliScanFileDataOrdered.Where(x => !x.IsDuplicatedForDuplicatedSheet).ToList();
            for (int i = 0; i < orderedData.Count; i++)
            {
                if (i == 0)
                {
                    prevoiusUserName = orderedData[i].UserName;
                    continue;
                }

                if (!orderedData[i].UserName?.Equals(prevoiusUserName) ?? false)
                {
                    ExcelInterop.Range line = (ExcelInterop.Range)finalPatientSummariesSheet.Rows[i + 3 + insertedRowCount];//3: 1 beccause of index zerobased, 2 the excel header 2 rows height
                    line.Insert();
                    prevoiusUserName = orderedData[i].UserName;

                    finalPatientSummariesSheet.Rows[2].Copy();
                    finalPatientSummariesSheet.Rows[i + 3 + insertedRowCount++].PasteSpecial();
                    Release(line);
                }
            }

            finalPatientSummariesSheet.Activate();
            finalPatientSummariesSheet.Cells[1, 1].Activate();

            #region Save and close

            try
            {
                workbook.Save();
                workbook.Close(true);
                workbooks.Close();
                application.Quit();

                Release(finalPatientSummariesSheet);
                Release(workbooksheets);
                Release(workbook);
                Release(workbooks);
                Release(window);
                Release(application);

                GC.Collect();
                GC.WaitForPendingFinalizers();

                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            catch (Exception exception)
            {
                Utilities.Instance.Log(LogLevels.Error, $"Error occured closing the excel.", exception);
            }

            #endregion save and close
        }

        private void Release(object obj)
        {
            try
            {
                if (obj != null)
                {
                    Marshal.ReleaseComObject(obj);
                    obj = null;
                }
            }
            catch { }
        }

        public void Dispose()
        {
            try
            {
                Process[] processes = Process.GetProcessesByName("EXCEL");
                foreach (Process process in processes)
                {
                    Process parent = process.GetParent();
                    if (parent.Id == Process.GetCurrentProcess().Id)
                    {
                        process.Kill();
                    }
                }
            }
            catch (Exception exception)
            {
                Utilities.Instance.Log(LogLevels.Error, $"Error occured killing the excel.", exception);
            }
        }
    }
}
