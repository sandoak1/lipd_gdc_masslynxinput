﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class SheetNamesPatient
    {
        public string SheetName { get; set; }

        public int Order { get; set; }
    }
}
