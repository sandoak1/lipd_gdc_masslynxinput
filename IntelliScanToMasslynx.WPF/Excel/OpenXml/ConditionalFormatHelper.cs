﻿
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public static class ConditionalFormatHelper
    {
        public const string RED = "FFFF0000";
        public const string ORANGE2 = "FFFABF8F";
        public const string WHITE = "FFFFFFFF";
        public const string ORANGE = "FFFFA500";
        public const string TRANSPARENT = "00ffffff";

        private static int _priority = 1;

        public static DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormatting CreateExpressionConditionalFormatting(string range, string expression)
        {
            DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormatting conditionalFormatting = new DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormatting();
            conditionalFormatting.AddNamespaceDeclaration("xm", "http://schemas.microsoft.com/office/excel/2006/main");

            DocumentFormat.OpenXml.Office.Excel.ReferenceSequence referenceSequence = new DocumentFormat.OpenXml.Office.Excel.ReferenceSequence();
            //referenceSequence.AddNamespaceDeclaration("xm", "http://schemas.microsoft.com/office/excel/2006/main");
            referenceSequence.Text = range;

            DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattingRule conditionalFormattingRule = new DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattingRule() { Type = ConditionalFormatValues.Expression, Priority = _priority++, StopIfTrue = true/*, Id = "{00000000-0000-0000-0000-000000000000}"*/ };
            DocumentFormat.OpenXml.Office.Excel.Formula formula1 = new DocumentFormat.OpenXml.Office.Excel.Formula();
            //formula1.AddNamespaceDeclaration("xne", "http://schemas.microsoft.com/office/excel/2006/main");
            //formula1.AddNamespaceDeclaration("xm", "http://schemas.microsoft.com/office/excel/2006/main");
            formula1.Text = expression;

            conditionalFormattingRule.Append(formula1);

            conditionalFormatting.Append(conditionalFormattingRule);
            conditionalFormatting.Append(referenceSequence);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateBetweenConditionalFormatting(string range, string min, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRule = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.Between, StopIfTrue = true };
            Formula formula1 = new Formula();
            formula1.Text = min;
            Formula formula2 = new Formula();
            formula2.Text = max;

            conditionalFormattingRule.Append(formula1);
            conditionalFormattingRule.Append(formula2);

            conditionalFormatting.Append(conditionalFormattingRule);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateBetweenConditionalFormattingWithExclusiveMin(string range, string min, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRuleLTOE = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.LessThanOrEqual };
            ConditionalFormattingRule conditionalFormattingRuleGT = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.GreaterThan };

            Formula formula1 = new Formula();
            formula1.Text = max;

            Formula formula2 = new Formula();
            formula2.Text = min;

            conditionalFormattingRuleLTOE.Append(formula1);
            conditionalFormattingRuleGT.Append(formula2);

            conditionalFormatting.Append(conditionalFormattingRuleLTOE);
            conditionalFormatting.Append(conditionalFormattingRuleGT);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateBetweenConditionalFormattingWithExclusiveMax(string range, string min, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRuleLT = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.LessThan, StopIfTrue = true };
            ConditionalFormattingRule conditionalFormattingRuleGTOE = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.GreaterThanOrEqual, StopIfTrue = true };

            Formula formula1 = new Formula();
            formula1.Text = max;

            Formula formula2 = new Formula();
            formula2.Text = min;

            conditionalFormattingRuleLT.Append(formula1);
            conditionalFormattingRuleGTOE.Append(formula2);

            conditionalFormatting.Append(conditionalFormattingRuleLT);
            conditionalFormatting.Append(conditionalFormattingRuleGTOE);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateBetweenConditionalFormattingWithExclusiveMinAndMax(string range, string min, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRuleLT = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.LessThan, StopIfTrue = true };
            ConditionalFormattingRule conditionalFormattingRuleGT = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.GreaterThan, StopIfTrue = true };

            Formula formula1 = new Formula();
            formula1.Text = max;

            Formula formula2 = new Formula();
            formula2.Text = min;

            conditionalFormattingRuleLT.Append(formula1);
            conditionalFormattingRuleGT.Append(formula2);

            conditionalFormatting.Append(conditionalFormattingRuleLT);
            conditionalFormatting.Append(conditionalFormattingRuleGT);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateGreaterThanOrEqualConditionalFormatting(string range, string min)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRule = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.GreaterThanOrEqual, StopIfTrue = true };
            Formula formula1 = new Formula();
            formula1.Text = min;

            conditionalFormattingRule.Append(formula1);

            conditionalFormatting.Append(conditionalFormattingRule);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateGreaterThanConditionalFormatting(string range, string min)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRule = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.GreaterThan, StopIfTrue = true };
            Formula formula1 = new Formula();
            formula1.Text = min;

            conditionalFormattingRule.Append(formula1);

            conditionalFormatting.Append(conditionalFormattingRule);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateLessThanOrEqualConditionalFormatting(string range, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRule = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.LessThanOrEqual, StopIfTrue = true };
            Formula formula1 = new Formula();
            formula1.Text = max;

            conditionalFormattingRule.Append(formula1);

            conditionalFormatting.Append(conditionalFormattingRule);

            return conditionalFormatting;
        }

        public static ConditionalFormatting CreateLessThanConditionalFormatting(string range, string max)
        {
            ConditionalFormatting conditionalFormatting = new ConditionalFormatting() { SequenceOfReferences = new ListValue<StringValue>() { InnerText = range } };

            ConditionalFormattingRule conditionalFormattingRule = new ConditionalFormattingRule() { Type = ConditionalFormatValues.CellIs, Priority = 1, Operator = ConditionalFormattingOperatorValues.LessThan, StopIfTrue = true };
            Formula formula1 = new Formula();
            formula1.Text = max;

            conditionalFormattingRule.Append(formula1);

            conditionalFormatting.Append(conditionalFormattingRule);

            return conditionalFormatting;
        }

        public static DifferentialFormat GetDifferentialFormat(HexBinaryValue color)
        {
            DifferentialFormat differentialFormat = new DifferentialFormat();
            Fill fill = new Fill();
            PatternFill patternFill = new PatternFill();

            BackgroundColor backgroundColor = new BackgroundColor() { Rgb = color };

            patternFill.Append(backgroundColor);
            fill.Append(patternFill);
            differentialFormat.Append(fill);

            return differentialFormat;
        }

        public static DocumentFormat.OpenXml.Office2010.Excel.DifferentialType GetDifferentialType(HexBinaryValue color)
        {
            DocumentFormat.OpenXml.Office2010.Excel.DifferentialType differentialType = new DocumentFormat.OpenXml.Office2010.Excel.DifferentialType();

            Fill fill = new Fill();
            PatternFill patternFill = new PatternFill();
            BackgroundColor backgroundColor = new BackgroundColor() { Rgb = color };

            patternFill.Append(backgroundColor);
            fill.Append(patternFill);
            differentialType.Append(fill);

            return differentialType;
        }
    }
}
