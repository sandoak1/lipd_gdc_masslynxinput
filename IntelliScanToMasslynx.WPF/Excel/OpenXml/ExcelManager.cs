﻿using DocumentFormat.OpenXml;
using C = DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Drawing.Spreadsheet;
using System.Diagnostics;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class ExcelManager : IDisposable
    {
        private string _documentPath;
        private string _templatePath;
        private bool _isValidTemplatePath;
        private SpreadsheetDocument _doc;
        private Dictionary<string, uint> _styleNamesAndIndexes = new Dictionary<string, uint>();
        private Dictionary<string, Cell> _cellReferencesWithWorksheetNameAndCells = new Dictionary<string, Cell>();
        private bool _hasTemplateHeader;
        private uint _defaultCellStyleIndex = 0;
        private bool _isSetDefaultCellStyleIndex = false;

        public static System.Diagnostics.Stopwatch _globalStopwatch = new System.Diagnostics.Stopwatch();
        public static int _CreateSpreadsheetCellIfNotExistMethodCalledCounter = 0;

        public ExcelManager(string documentPath, string templatePath = null, bool hasTemplateHeader = false)
        {
            if (String.IsNullOrWhiteSpace(documentPath))
            {
                throw new ArgumentNullException("The document path cannot be null or empty.");
            }
            if (!Path.GetExtension(documentPath).Equals(".xlsx"))
            {
                throw new ArgumentException($"The document file is not an excel file. The path: '{documentPath}'", nameof(documentPath));
            }

            if (!String.IsNullOrWhiteSpace(templatePath) && Path.GetExtension(templatePath).Equals(".xlsx") && File.Exists(templatePath))
            {
                this._isValidTemplatePath = true;
            }
            if (!this._isValidTemplatePath && !File.Exists(documentPath))
            {
                throw new FileNotFoundException("The given document file does not exists.", documentPath);
            }

            this._documentPath = documentPath;
            this._templatePath = templatePath;
            this._hasTemplateHeader = hasTemplateHeader;

            if (this._isValidTemplatePath)
            {
                if (File.Exists(documentPath))
                {
                    File.Delete(documentPath);
                }
                File.Copy(templatePath, documentPath, true); 
            }

            try
            {
                this._doc = SpreadsheetDocument.Open(documentPath, this._isValidTemplatePath);
            }
            catch (OpenXmlPackageException exception)
            {
                if (this._isValidTemplatePath)
                {
                    this._doc = SpreadsheetDocument.Create(documentPath, SpreadsheetDocumentType.Workbook);
                }
            }

            if (this._isValidTemplatePath && !this._hasTemplateHeader)
            {
                AddDefaultStyle(); 
            }

            if (this._isValidTemplatePath)
            {
                NumberFormatHelper.GENERAL_NUMBERFORMAT_CUSTOM = this.AddCustomNumberingCellFormatToTheStylesheet(NumberFormatHelper.GENERAL_NUMBERFORMAT_CUSTOM, "#0");
                NumberFormatHelper.GENERAL_NUMBERFORMAT_CUSTOMDATE = this.AddCustomNumberingCellFormatToTheStylesheet(NumberFormatHelper.GENERAL_NUMBERFORMAT_CUSTOMDATE, "[$-409]d\\-mmm\\-yyyy;@");
            }
        }

        public string DocumentPath
        {
            get
            {
                return this._documentPath;
            }
        }

        // Given a document name, inserts a new worksheet.
        public void CreateWorksheet(string worksheetName)
        {
            try
            {
                Worksheet worksheet = this.GetWorksheet(worksheetName);
                if (worksheet != null)
                {
                    if (!worksheet.Elements<WorksheetExtensionList>().Any() && worksheetName.ToLower().Equals("bw"))
                    {
                        WorksheetExtensionList worksheetExtensionList1 = new WorksheetExtensionList();
                        WorksheetExtension worksheetExtension1 = new WorksheetExtension() { Uri = "{78C0D931-6437-407d-A8EE-F0AAD7539E65}" };
                        worksheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");

                        worksheetExtensionList1.Append(worksheetExtension1);
                        worksheet.Append(worksheetExtensionList1);
                    }
                    return;
                }
            }
            catch (Exception exception)
            {

            }

            if (this._doc.WorkbookPart == null)
            {
                this._doc.AddWorkbookPart();
                this._doc.WorkbookPart.Workbook = new Workbook();
            }
            WorksheetPart newWorksheetPart = this._doc.WorkbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = this._doc.WorkbookPart.Workbook.GetFirstChild<Sheets>();
            if (sheets == null)
            {
                sheets = this._doc.WorkbookPart.Workbook.AppendChild(new Sheets());
            }
            string relationshipId = this._doc.WorkbookPart.GetIdOfPart(newWorksheetPart);

            // Get a unique ID for the new worksheet.
            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            // Append the new worksheet and associate it with the workbook.
            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = worksheetName };
            sheets.Append(sheet);

            if (!newWorksheetPart.Worksheet.Elements<WorksheetExtensionList>().Any() && worksheetName.ToLower().Equals("bw"))
            {
                if (!newWorksheetPart.Worksheet.Elements<WorksheetExtensionList>().Any())
                {
                    WorksheetExtensionList worksheetExtensionList1 = new WorksheetExtensionList();
                    WorksheetExtension worksheetExtension1 = new WorksheetExtension() { Uri = "{78C0D931-6437-407d-A8EE-F0AAD7539E65}" };
                    //worksheetExtension1.AddNamespaceDeclaration("x14", "http://schemas.microsoft.com/office/spreadsheetml/2009/9/main");

                    worksheetExtensionList1.Append(worksheetExtension1);
                    newWorksheetPart.Worksheet.Append(worksheetExtensionList1);
                }
            }

            newWorksheetPart.Worksheet.Save();
            this._doc.WorkbookPart.Workbook.Save();
        }

        public Stylesheet GetStylesheet()
        {
            WorkbookStylesPart stylesPart;
            if (this._doc.WorkbookPart.WorkbookStylesPart == null)
            {
                stylesPart = this._doc.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            }

            stylesPart = this._doc.WorkbookPart.WorkbookStylesPart;

            if (stylesPart.Stylesheet == null)
            {
                stylesPart.Stylesheet = new Stylesheet(); 
            }

            return stylesPart.Stylesheet;
        }

        private void AddDefaultStyle()
        {
            WorkbookStylesPart stylesPart;
            if (this._doc.WorkbookPart.WorkbookStylesPart == null)
            {
                stylesPart = this._doc.WorkbookPart.AddNewPart<WorkbookStylesPart>();
            }

            stylesPart = this._doc.WorkbookPart.WorkbookStylesPart;

            stylesPart.Stylesheet = new Stylesheet();
            Stylesheet stylesheet = stylesPart.Stylesheet;

            stylesheet.Fonts = new Fonts();
            stylesheet.Fonts.Count = 1;
            stylesheet.Fonts.AppendChild(new Font());

            stylesheet.Fills = new Fills();

            stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } }); // required, reserved by Excel
            stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } }); // required, reserved by Excel
            stylesheet.Fills.Count = 2;

            stylesheet.Borders = new Borders();
            stylesheet.Borders.Count = 1;
            stylesheet.Borders.AppendChild(new Border { LeftBorder = new LeftBorder(), TopBorder = new TopBorder(), RightBorder = new RightBorder(), BottomBorder = new BottomBorder(), DiagonalBorder = new DiagonalBorder() });

            stylesheet.NumberingFormats = new NumberingFormats();
            stylesheet.NumberingFormats.AppendChild(new NumberingFormat { NumberFormatId = NumberFormatHelper.NUMBERFORMATID, FormatCode = "#,##0.0" });
            stylesheet.NumberingFormats.AppendChild(new NumberingFormat { NumberFormatId = NumberFormatHelper.DATEFORMATID, FormatCode = "DD-MMM" });
            stylesheet.NumberingFormats.Count = 2;

            stylesheet.CellStyleFormats = new CellStyleFormats();
            stylesheet.CellStyleFormats.Count = 1;
            stylesheet.CellStyleFormats.AppendChild(new CellFormat());

            stylesheet.CellFormats = new CellFormats();
            stylesheet.CellFormats.AppendChild(new CellFormat());
            stylesheet.CellFormats.Count = 1;

            stylesheet.CellStyles = new CellStyles();
            stylesheet.CellStyles.AppendChild(new CellStyle { Name = "Normal", FormatId = 0 });
            stylesheet.CellStyles.Count = 1;

            stylesheet.DifferentialFormats = new DifferentialFormats();
            stylesheet.DifferentialFormats.AppendChild(new DifferentialFormat());
            stylesheet.DifferentialFormats.Count = 1;

            stylesheet.Save();
        }

        public uint AddCustomNumberingCellFormatToTheStylesheet(uint formatId, string formatCode)
        {
            WorkbookStylesPart stylesPart = this._doc.WorkbookPart.WorkbookStylesPart;

            if (stylesPart == null)
            {
                throw new Exception("The stylesheetPart cannot be null.");
            }

            Stylesheet stylesheet = stylesPart.Stylesheet;
            if (stylesheet == null)
            {
                throw new Exception("The stylesheet cannot be null.");
            }

            if (stylesheet.NumberingFormats == null)
            {
                stylesheet.NumberingFormats = new NumberingFormats();
                stylesheet.NumberingFormats.Count = 0;
            }

            NumberingFormat existingNumberingFormat = stylesheet.NumberingFormats.FirstOrDefault(x => (x as NumberingFormat)?.FormatCode?.Value?.Equals(formatCode) ?? false) as NumberingFormat;
            if (existingNumberingFormat == null)
            {
                stylesheet.NumberingFormats.AppendChild(new NumberingFormat { NumberFormatId = formatId, FormatCode = formatCode });
                stylesheet.NumberingFormats.Count += 1;
                stylesheet.Save();
                return formatId;
            }
            else
            {
                stylesheet.Save();
                return existingNumberingFormat.NumberFormatId;
            }
        }

        public void AddCellFormat(string name, CellFormatComponents cellFormatComponents)
        {
            if (this._styleNamesAndIndexes.ContainsKey(name))
            {
                throw new Exception("This style exists.");
            }

            Stylesheet stylesheet = this._doc.WorkbookPart.WorkbookStylesPart.Stylesheet;
            bool isCreatedNewItem = false;
            uint fontId = 0;
            uint borderId = 0;
            uint fillId = 0;


            var asdawda = stylesheet.Fonts.Where(x => x.InnerXml == "<x:sz val=\"11\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:color theme=\"1\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:name val=\"Arial\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:family val=\"2\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" />").ToList();
            List<int> indexes = new List<int>();
            for (int i = 0; i < stylesheet.Fonts.Count; i++)
            {
                if (stylesheet.Fonts.ElementAt(i).InnerXml.Equals("<x:sz val=\"11\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:color theme=\"1\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:name val=\"Arial\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" /><x:family val=\"2\" xmlns:x=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" />"))
                {
                    indexes.Add(i);
                }
            }

            if (cellFormatComponents.Font != null)
            {
                var fonts = stylesheet.Fonts.ToList().Where(x => x.InnerXml == cellFormatComponents.Font.InnerXml).ToList();
                if (fonts.Count == 0)
                {
                    try
                    {
                        stylesheet.Fonts.AppendChild(cellFormatComponents.Font);
                    }
                    catch (InvalidOperationException invalidOperationException)
                    {
                        Font clonedFont = cellFormatComponents.Font.CloneNode(true) as Font;
                        stylesheet.Fonts.AppendChild(clonedFont);
                    }
                    stylesheet.Fonts.Count++;
                    fontId = stylesheet.Fonts.Count - 1;
                    isCreatedNewItem = true;
                }
                else if (fonts.Count == 1)
                {
                    fontId = (uint)stylesheet.Fonts.ToList().IndexOf(fonts.First());
                }
                else
                {
                    fontId = (uint)stylesheet.Fonts.ToList().IndexOf(fonts.First());
                    //throw new Exception("more fonts");
                }
            }

            if (cellFormatComponents.Border != null)
            {
                var borders = stylesheet.Borders.ToList().Where(x => x.InnerXml == cellFormatComponents.Border.InnerXml).ToList();
                if (borders.Count == 0)
                {
                    try
                    {
                        stylesheet.Borders.AppendChild(cellFormatComponents.Border);
                    }
                    catch (InvalidOperationException invalidOperationException)
                    {
                        Border clonedBorder = cellFormatComponents.Border.CloneNode(true) as Border;
                        stylesheet.Borders.AppendChild(clonedBorder);
                    }
                    stylesheet.Borders.Count++;
                    borderId = stylesheet.Borders.Count - 1;
                    isCreatedNewItem = true;
                }
                else if (borders.Count == 1)
                {
                    borderId = (uint)stylesheet.Borders.ToList().IndexOf(borders.First());
                }
                else
                {
                    throw new Exception("more borders");
                }
            }

            if (cellFormatComponents.Fill != null)
            {
                var fills = stylesheet.Fills.ToList().Where(x => x.InnerXml == cellFormatComponents.Fill.InnerXml).ToList();
                if (fills.Count == 0)
                {
                    try
                    {
                        stylesheet.Fills.AppendChild(cellFormatComponents.Fill);
                    }
                    catch (InvalidOperationException invalidOperationException)
                    {
                        Fill clonedFill = cellFormatComponents.Fill.CloneNode(true) as Fill;
                        stylesheet.Fills.AppendChild(clonedFill);
                    }
                    stylesheet.Fills.Count++;
                    fillId = stylesheet.Fills.Count - 1;
                    isCreatedNewItem = true;
                }
                else if (fills.Count == 1)
                {
                    fillId = (uint)stylesheet.Fills.ToList().IndexOf(fills.First());
                }
                else
                {
                    throw new Exception("more fills");
                }
            }

            if (isCreatedNewItem)
            {
                CellFormat cellformat = new CellFormat
                {
                    FontId = fontId,
                    BorderId = borderId,
                    FillId = fillId,
                    Alignment = cellFormatComponents?.Alignment?.CloneNode(true) as Alignment,
                    NumberFormatId = cellFormatComponents.NumberFormatId
                };
                stylesheet.CellFormats.AppendChild(cellformat);
                stylesheet.CellFormats.Count++;


                this._styleNamesAndIndexes.Add(name, stylesheet.CellFormats.Count - 1);
            }
            else
            {
                CellFormat cellFormat = (CellFormat)stylesheet.CellFormats.SingleOrDefault(x =>
                {
                    bool b = false;
                    CellFormat cf = x as CellFormat;
                    try
                    {
                        b = cf.FontId == fontId && cf.BorderId == borderId && cf.FillId == fillId
                                    && cf.Alignment.Horizontal == cellFormatComponents.Alignment.Horizontal && cf.Alignment.Vertical == cellFormatComponents.Alignment.Vertical && cf.Alignment.Indent == cellFormatComponents.Alignment.Indent
                                    && cf.Alignment.JustifyLastLine == cellFormatComponents.Alignment.JustifyLastLine && cf.Alignment.MergeCell == cellFormatComponents.Alignment.MergeCell && cf.Alignment.ReadingOrder == cellFormatComponents.Alignment.ReadingOrder
                                    && cf.Alignment.RelativeIndent == cellFormatComponents.Alignment.RelativeIndent && cf.Alignment.ShrinkToFit == cellFormatComponents.Alignment.ShrinkToFit && cf.Alignment.TextRotation == cellFormatComponents.Alignment.TextRotation
                                    && cf.Alignment.WrapText == cellFormatComponents.Alignment.WrapText;
                    }
                    catch (Exception exception)
                    {

                    }
                    return b;
                });

                if (cellFormat == null)
                {
                    stylesheet.CellFormats.AppendChild(new CellFormat
                    {
                        FontId = fontId,
                        BorderId = borderId,
                        FillId = fillId,
                        Alignment = cellFormatComponents?.Alignment?.CloneNode(true) as Alignment,
                        NumberFormatId = cellFormatComponents.NumberFormatId,
                    });
                    stylesheet.CellFormats.Count++;
                    this._styleNamesAndIndexes.Add(name, stylesheet.CellFormats.Count - 1);
                }
                else
                {
                    throw new Exception("This cellformat exists with another name.");

                }
            }

            this._doc.WorkbookPart.Workbook.Save();
        }

        public void ApplyCellFormat(Cell cell, string formatName)
        {
            if (!this._styleNamesAndIndexes.ContainsKey(formatName))
            {
                throw new Exception("There is no cellformat with the given name.");
            }

            cell.StyleIndex = this._styleNamesAndIndexes[formatName];
        }

        public Workbook GetWorkbook()
        {
            return this._doc.WorkbookPart.Workbook;
        }

        public WorksheetPart GetWorksheetPart(string worksheetName)
        {
            Sheet sheet = this._doc.WorkbookPart.Workbook.Descendants<Sheet>().SingleOrDefault(x => x.Name == worksheetName);
            if (sheet == null)
            {
                throw new Exception("There is no sheet with the given name.");
            }

            string relId = sheet.Id;
            WorksheetPart worksheetPart = (WorksheetPart)this._doc.WorkbookPart.GetPartById(relId);

            if (worksheetPart == null)
            {
                throw new Exception("There is no worksheetpart with the given name.");
            }

            return worksheetPart;
        }

        public Worksheet GetWorksheet(string worksheetName)
        {
            if (String.IsNullOrWhiteSpace(worksheetName))
            {
                throw new ArgumentNullException(nameof(worksheetName), "The name of the worksheet cannot be null.");
            }

            WorksheetPart worksheetPart = this.GetWorksheetPart(worksheetName);
            Worksheet worksheet = worksheetPart.Worksheet;
            if (worksheet.ChildElements == null || worksheet.ChildElements.Count == 0)
            {
                worksheet.AppendChild(new SheetData());
            }

            return worksheet;
        }

        /// <summary>
        /// Returns the worksheet
        /// </summary>
        /// <param name="number">The ordinal number of the worksheet.</param>
        /// <returns></returns>
        public Worksheet GetWorksheet(int number)
        {
            Workbook workbook = this._doc.WorkbookPart.Workbook;
            Sheet sheet = workbook.Descendants<Sheet>().ElementAtOrDefault(number);
            if (sheet == null)
            {
                throw new Exception($"The file does not contain the {number}. worksheet.");
            }
            WorksheetPart worksheetPart = (WorksheetPart)workbook.WorkbookPart.GetPartById(sheet.Id);
            Worksheet worksheet = worksheetPart.Worksheet;
            if (worksheet == null)
            {
                throw new Exception("The file does not contain worksheet.");
            }

            return worksheet;
        }

        public AutoFilter GetAutoFilter(string worksheetName)
        {
            if (String.IsNullOrWhiteSpace(worksheetName))
            {
                throw new ArgumentNullException(nameof(worksheetName), "The name of the worksheet cannot be null.");
            }

            Worksheet worksheet = this.GetWorksheet(worksheetName);
            IEnumerable<AutoFilter> autoFilters = worksheet.Elements<AutoFilter>();

            if (autoFilters.Count() != 1)
            {
                throw new Exception($"The worksheet '{worksheetName}' has more than one filters.");
            }

            return autoFilters.First();
        }

        /// <summary>
        /// Returns the sheetdata
        /// </summary>
        /// <param name="number">The ordinal number of the worksheet.</param>
        /// <returns></returns>
        public string GetWorksheetName(int number)
        {
            if (number < 1)
            {
                throw new ArgumentException("The excel should have been at least one worksheet. Please give at least 1.");
            }

            //Worksheet worksheet = this.GetWorksheet(number);
            Sheet sheet = this._doc.WorkbookPart.Workbook.Descendants<Sheet>().ElementAtOrDefault(number - 1);
            if (sheet == null)
            {
                throw new Exception($"The file does not have {number}. worksheets.");
            }

            return sheet.Name;
        }

        /// <summary>
        /// Returns worksheetnames
        /// </summary>
        /// <returns></returns>
        public IList<string> GetWorksheetNames()
        {
            IEnumerable<Sheet> sheets = this._doc.WorkbookPart.Workbook.Descendants<Sheet>();
            if (sheets == null || sheets.Count() == 0)
            {
                throw new Exception($"The file does not have any worksheets.");
            }

            IList<string> sheetNames = sheets.Select(x => x.Name.Value).ToList();

            return sheetNames;
        }

        /// <summary>
        /// Returns the sheetdata
        /// </summary>
        /// <param name="number">The ordinal number of the worksheet.</param>
        /// <returns></returns>
        public SheetData GetSheetData(int number)
        {
            //Worksheet worksheet = this.GetWorksheet(number);
            Sheet sheet = this._doc.WorkbookPart.Workbook.Descendants<Sheet>().ElementAtOrDefault(number);
            if (sheet == null)
            {
                throw new Exception($"The file doen not have {number}. worksheets.");
            }

            SheetData sheetData = this.GetSheetData(sheet.Name);
            return sheetData;
        }

        public SheetData GetSheetData(string worksheetName)
        {
            Worksheet worksheet = this.GetWorksheet(worksheetName);

            foreach (var item in worksheet.ChildElements)
            {
                if (item is SheetData)
                {
                    return item as SheetData;
                }
            }

            throw new Exception("There is no sheetdata.");
        }

        public Row GetRow(string worksheetName, int rowIndex)
        {
            SheetData sheetData = this.GetSheetData(worksheetName);
            Row row = sheetData.Elements<Row>().SingleOrDefault(r => r.RowIndex == rowIndex);

            return row;
        }

        public IEnumerable<Row> GetRows(string worksheetName)
        {
            SheetData sheetData = this.GetSheetData(worksheetName);
            IEnumerable<Row> rows = sheetData.Elements<Row>();

            return rows;
        }

        public int GetRowCount(string worksheetName)
        {
            SheetData sheetData = this.GetSheetData(worksheetName);
            int count = sheetData.Elements<Row>().Count();

            return count;
        }

        public IList<Cell> GetRowCells(Row row)
        {
            if (row == null)
            {
                throw new ArgumentNullException(nameof(row), "The row cannot be null.");
            }

            IList<Cell> cells = row.Elements<Cell>().ToList();
            return cells;
        }

        public IList<CellValuePatient> GetRowCellValues(Row row)
        {
            if (row == null)
            {
                throw new ArgumentNullException(nameof(row), "The row cannot be null.");
            }

            List<CellValuePatient> rowCellValues = new List<CellValuePatient>();

            foreach (Cell cell in row.Elements<Cell>())
            {
                CellValuePatient cellValue = this.GetCellValue(cell);
                rowCellValues.Add(cellValue);
            }

            return rowCellValues;
        }

        public Cell GetCell(string worksheetName, CellData cellData)
        {
            CellData newCellData = new CellData(cellData.CellReference, false, worksheetName);
            if (this._cellReferencesWithWorksheetNameAndCells.ContainsKey(newCellData.CellReferenceWithSheetName))
            {
                return this._cellReferencesWithWorksheetNameAndCells[newCellData.CellReferenceWithSheetName];
            }

            SheetData sheetData = this.GetSheetData(worksheetName);
            Row row = sheetData.Elements<Row>().SingleOrDefault(r => r.RowIndex == cellData.RowIndex);

            if (row == null)
            {
                return null;
            }

            Cell cell = row.Elements<Cell>().SingleOrDefault(c => c.CellReference == cellData.CellReference);

            return cell;
        }

        // Retrieve the value of a cell, given a file name, sheet name, and address name.
        public CellValuePatient GetCellValue(string sheetName, string addressName)
        {
            Cell cell = this.GetCell(sheetName, new CellData(addressName, false));

            // If the cell does not exist, return an empty string.
            if (cell == null)
            {
                return null;
            }

            CellValuePatient value = this.GetCellValue(cell);
            
            return value;
        }

        public CellValuePatient GetCellValue(Cell cell)
        {
            CellValuePatient value = null;

            if (cell == null)
            {
                return value;
            }

            value = new CellValuePatient();
            value.Value = cell.InnerText;

            // If the cell represents an integer number, you are done. For dates, this code returns the serialized value that represents the date. The code handles strings and 
            //Booleans individually. For shared strings, the code looks up the corresponding value in the shared string table. For Booleans, the code converts the value into the words TRUE or FALSE.
            if (cell.DataType != null)
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:

                        // For shared strings, look up the value in the shared strings table.
                        var stringTable = this._doc.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                        // If the shared string table is missing, something is wrong. Return the index that is in the cell. Otherwise, look up the correct text in the table.
                        if (stringTable != null)
                        {
                            SharedStringItem sharedStringItem = stringTable.SharedStringTable.ElementAt(int.Parse(value.Value)) as SharedStringItem;
                            value.Value = sharedStringItem.InnerText;
                            value.OuterXml = sharedStringItem.OuterXml;
                            value.CellValues = CellValues.SharedString;
                        }
                        break;
                    case CellValues.String:
                        var stringTable02 = this._doc.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

                        int mm = 7;

                        break;
                    case CellValues.Boolean:
                        switch (value.Value)
                        {
                            case "0":
                                value.Value = "FALSE";
                                break;
                            default:
                                value.Value = "TRUE";
                                break;
                        }
                        break;
                }
            }
            return value;
        }

        public CellValuePatient GetCellValue(CellData cellData)
        {
            return this.GetCellValue(cellData.WorksheetName, cellData.CellReference);
        }

        public CellFormula GetCellFormula(Cell cell)
        {
            if (cell == null)
            {
                return null;
            }

            if (cell.CellFormula != null)
            {
                return new CellFormula(cell.CellFormula.Text);
            }

            return null;
        }

        public void SaveWorksheet(string worksheetName)
        {
            Worksheet worksheet = this.GetWorksheet(worksheetName);
            worksheet.Save();
        }

        public void ApplyConditionalFormat(string worksheetName, ConditionalFormatting conditionalFormatting, DifferentialFormat differentialFormat)
        {
            Stylesheet stylesheet = this._doc.WorkbookPart.WorkbookStylesPart.Stylesheet;
            stylesheet.DifferentialFormats.AppendChild(differentialFormat);
            stylesheet.DifferentialFormats.Count++;

            ConditionalFormattingRule conditionalFormattingRule = conditionalFormatting.FirstChild as ConditionalFormattingRule;
            if (conditionalFormattingRule == null)
            {
                throw new Exception("Invalid conditional formatting rule.");
            }
            conditionalFormattingRule.FormatId = stylesheet.DifferentialFormats.Count - 1;

            Worksheet worksheet = this.GetWorksheet(worksheetName);
            worksheet.Append(conditionalFormatting);
        }

        public void ApplyConditionalFormatWithFormula(string worksheetName, DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormatting conditionalFormatting, DocumentFormat.OpenXml.Office2010.Excel.DifferentialType differentialType)
        {
            conditionalFormatting.Elements<DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattingRule>().Single().AppendChild(differentialType);

            Worksheet worksheet = this.GetWorksheet(worksheetName);

            WorksheetExtension worksheetExtension = worksheet.Elements<WorksheetExtensionList>().Single().Elements<WorksheetExtension>().Single();
            if (!worksheetExtension.Elements<DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattings>().Any())
            {
                worksheetExtension.AppendChild(new DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattings());
            }

            DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattings conditionalFormattings = worksheetExtension.Elements<DocumentFormat.OpenXml.Office2010.Excel.ConditionalFormattings>().Single();
            conditionalFormattings.AppendChild(conditionalFormatting);
        }

        // Given a Worksheet and a cell name, verifies that the specified cell exists.
        // If it does not exist, creates a new cell. 
        public void CreateSpreadsheetCellIfNotExist(string worksheetName, Cell cell)
        {
            System.Diagnostics.Stopwatch localStopwatch = new System.Diagnostics.Stopwatch();
            _CreateSpreadsheetCellIfNotExistMethodCalledCounter++;
            _globalStopwatch.Start();
            localStopwatch.Start();



            if (cell == null)
            {
                throw new ArgumentNullException("The cell cannot be null.");
            }

            CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
            if (this._cellReferencesWithWorksheetNameAndCells.ContainsKey(cellData.CellReferenceWithSheetName))
            {
                Cell existingCell = this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName];

                try
                {
                    Row row = existingCell.Parent as Row;
                    row.ReplaceChild(cell, existingCell);
                    cell.StyleIndex = existingCell.StyleIndex;
                    this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                }
                catch (InvalidOperationException invalidOperationException)
                {
                    //The newCell is already in the row. Do nothing.
                }

                return;
            }

            Worksheet worksheet = this.GetWorksheet(worksheetName);
            string columnName = Helper.GetColumnName(cell.CellReference);
            uint rowIndex = Helper.GetRowIndex(cell.CellReference);

            IEnumerable<Row> rows = worksheet.Elements<SheetData>().Single().Elements<Row>().Where(r => r.RowIndex.Value == rowIndex);

            // If the Worksheet does not contain the specified row, create the specified row.
            // Create the specified cell in that row, and insert the row into the Worksheet.
            if (!rows.Any())
            {
                Row newRow = new Row() { RowIndex = new UInt32Value(rowIndex) };
                newRow.Append(cell);

                Row firstNotEmptyRow = worksheet.Elements<SheetData>().Single().Elements<Row>().FirstOrDefault(r => r.RowIndex.Value > rowIndex);
                if (firstNotEmptyRow == null)
                {
                    worksheet.Elements<SheetData>().Single().Append(newRow);
                }
                else
                {
                    firstNotEmptyRow.InsertBeforeSelf(newRow);
                }
            }
            else
            {
                Row row = rows.First();

                IEnumerable<Cell> cells = row.Elements<Cell>().Where(c => c.CellReference.Value == cell.CellReference.Value);

                // If the row does not contain the specified cell, create the specified cell.
                if (!cells.Any())
                {
                    Cell firtNotEmptyCell = row.Elements<Cell>().FirstOrDefault(c => Helper.GetColumnIndexByCellName(c.CellReference.Value) > Helper.GetColumnIndexByCellName(cell.CellReference.Value));

                    if (firtNotEmptyCell == null)
                    {
                        row.Append(cell);
                    }
                    else
                    {
                        firtNotEmptyCell.InsertBeforeSelf(cell);
                    }
                }
                else if (cells != null && cells.Count() == 1)
                {
                    Cell existingCell = cells.First();
                    try
                    {
                        cell.StyleIndex = existingCell.StyleIndex;
                    }
                    catch (Exception exception)
                    {

                    }
                    cells.First().Parent.ReplaceChild(cell, cells.First());
                }
            }

            this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;

            localStopwatch.Stop();
            _globalStopwatch.Stop();
            Debug.WriteLine("CreateSpreadsheetCellIfNotExist method elapsed: " + localStopwatch.Elapsed);
        }

        // Given a document name, a worksheet name, and the names of two adjacent cells, merges the two cells.
        // When two cells are merged, only the content from one cell is preserved:
        // the upper-left cell for left-to-right languages or the upper-right cell for right-to-left languages.
        public void MergeTwoCells(string sheetName, Cell cell1, Cell cell2)
        {
            if (cell1.CellReference.Value == cell2.CellReference.Value)
            {
                return;
            }

            Worksheet worksheet = this.GetWorksheet(sheetName);

            for (uint i = Helper.GetRowIndex(cell1.CellReference.Value); i <= Helper.GetRowIndex(cell2.CellReference.Value); i++)
            {
                for (int j = Helper.GetColumnIndexByColumnName(Helper.GetColumnName(cell1.CellReference.Value)); j <= Helper.GetColumnIndexByColumnName(Helper.GetColumnName(cell2.CellReference.Value)); j++)
                {
                    Cell cell = this.GetCell(sheetName, new CellData((int)i, j, false));

                    if (cell == null)
                    {
                        Cell actualCell = new Cell() { CellReference = Helper.GetCellName((int)i, j), StyleIndex = cell1.StyleIndex };
                        this.CreateSpreadsheetCellIfNotExist(sheetName, actualCell);
                    }
                    else
                    {
                        cell.StyleIndex = cell1.StyleIndex;
                    }
                }
            }

            MergeCells mergeCells;
            if (worksheet.Elements<MergeCells>().Count() > 0)
            {
                mergeCells = worksheet.Elements<MergeCells>().First();
            }
            else
            {
                mergeCells = new MergeCells();

                // Insert a MergeCells object into the specified position.
                if (worksheet.Elements<CustomSheetView>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<CustomSheetView>().First());
                }
                else if (worksheet.Elements<DataConsolidate>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<DataConsolidate>().First());
                }
                else if (worksheet.Elements<SortState>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<SortState>().First());
                }
                else if (worksheet.Elements<AutoFilter>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<AutoFilter>().First());
                }
                else if (worksheet.Elements<Scenarios>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<Scenarios>().First());
                }
                else if (worksheet.Elements<ProtectedRanges>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<ProtectedRanges>().First());
                }
                else if (worksheet.Elements<SheetProtection>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetProtection>().First());
                }
                else if (worksheet.Elements<SheetCalculationProperties>().Count() > 0)
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetCalculationProperties>().First());
                }
                else
                {
                    worksheet.InsertAfter(mergeCells, worksheet.Elements<SheetData>().Single());
                }
            }

            // Create the merged cell and append it to the MergeCells collection.
            MergeCell mergeCell = new MergeCell() { Reference = new StringValue(cell1.CellReference.Value + ":" + cell2.CellReference.Value) };
            mergeCells.Append(mergeCell);
        }

        public void UpdateLineChart(string chartTitle, string actualWorkingWorksheetName, string chartContainingWorksheetName, IEnumerable<C.LineChartSeries> series, int startRowPosition)
        {
            WorksheetPart worksheetpart = this.GetWorksheetPart(chartContainingWorksheetName);

            DrawingsPart drawingsPart;
            if (worksheetpart.DrawingsPart == null)
            {
                drawingsPart = worksheetpart.AddNewPart<DrawingsPart>();
            }
            drawingsPart = worksheetpart.DrawingsPart;

            ChartPart chartPart = drawingsPart.ChartParts.Single(x =>
            {
                return x.ChartSpace.Elements<C.Chart>().First().Descendants<C.Title>().First().InnerText.Equals(chartTitle);
            });

            int chartPositionIndex = drawingsPart.ChartParts.ToList().IndexOf(chartPart);

            C.Chart chart = chartPart.ChartSpace.Elements<C.Chart>().First();

            ChartHelper.UpdateChartSerie2(chart, series);

            TwoCellAnchor twoCellAnchor = drawingsPart.WorksheetDrawing.Elements<TwoCellAnchor>().ElementAt(chartPositionIndex);

            int currentFromRowIndex = Int32.Parse(twoCellAnchor.FromMarker.RowId.Text);
            int currentToRowIndex = Int32.Parse(twoCellAnchor.ToMarker.RowId.Text);

            twoCellAnchor.FromMarker.RowId = new RowId((currentFromRowIndex + startRowPosition).ToString());

            twoCellAnchor.ToMarker.RowId = new RowId((currentToRowIndex + startRowPosition).ToString());


            worksheetpart.Worksheet.Save();
            drawingsPart.WorksheetDrawing.Save();
        }

        public void CreateDefaultCells(string worksheetName, uint rowCount, int columnCount)
        {
            SheetData worksheetData = this.GetSheetData(worksheetName);
            //worksheetData.RemoveAllChildren();

            uint startRowIndex = 1;
            if (this._hasTemplateHeader)
            {
                startRowIndex = 2;

                uint styleIndex = 0;
                bool isSetStyleIndex = false;

                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
                {
                    Cell cell = this.GetCell(worksheetName, new CellData(1, columnIndex, false));

                    CellData cellData;
                    if (cell == null)
                    {
                        string cellReference = Helper.GetCellName(1, columnIndex);
                        cellData = new CellData(cellReference, false, worksheetName);
                        cell = new Cell() { CellReference = cellData.CellReference, CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(""), DataType = CellValues.String, StyleIndex = styleIndex };

                        Row headerRow = this.GetRow(worksheetName, 1);
                        headerRow.AppendChild(cell);
                    }
                    else
                    {
                        if (!isSetStyleIndex)
                        {
                            styleIndex = cell.StyleIndex;
                            isSetStyleIndex = true;
                        }
                        cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                    }

                    this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                }
            }

            uint maxRowIndexValue = this.GetRows(worksheetName).Max(x => x.RowIndex.Value);

            for (uint rowIndex = startRowIndex; rowIndex <= rowCount; rowIndex++)
            {
                Row existingRow = this.GetRow(worksheetName, (int)rowIndex);
                if (existingRow == null)
                {
                    Row row = new Row() { RowIndex = new UInt32Value(rowIndex) };

                    for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
                    {
                        string cellReference = Helper.GetCellName((int)rowIndex, columnIndex);
                        CellData cellData = new CellData(cellReference, false, worksheetName);
                        Cell cell = new Cell() { CellReference = cellData.CellReference, CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(""), DataType = CellValues.String, StyleIndex = this._defaultCellStyleIndex };
                        row.AppendChild(cell);
                        this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                    }

                    if (rowIndex > maxRowIndexValue)
                    {
                        worksheetData.AppendChild(row);
                    }
                    else
                    {
                        Row existingBeforeRow = this.GetRow(worksheetName, (int)(rowIndex - 1));
                        existingBeforeRow.InsertAfterSelf(row);
                    }

                }
                else
                {
                    IList<Cell> existingRowCells = this.GetRowCells(existingRow);
                    if (!this._isSetDefaultCellStyleIndex && existingRowCells.FirstOrDefault() != null)
                    {
                        this._defaultCellStyleIndex = existingRowCells.First().StyleIndex;
                        this._isSetDefaultCellStyleIndex = true;
                    }
                    for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++)
                    {
                        string cellName = Helper.GetCellName((int)existingRow.RowIndex.Value, columnIndex);
                        if (existingRowCells.Count(x => x.CellReference.Value.Equals(cellName)) == 1)
                        {
                            Cell existingCell = existingRowCells.FirstOrDefault(x => x.CellReference.Value.Equals(cellName));
                            CellData cellData = new CellData(existingCell.CellReference.Value, false, worksheetName);
                            this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = existingCell;
                            continue;
                        }

                        if (columnIndex == 1 && existingRowCells.Count == 0)
                        {
                            Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                            existingRow.AppendChild(cell);

                            CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                            this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                        }
                        else if (columnIndex == 1 && existingRowCells.Count > 0)
                        {
                            Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                            existingRowCells[0].InsertBeforeSelf(cell);

                            CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                            this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                        }
                        else
                        {
                            IList<Cell> existingRowCellsUpdated = this.GetRowCells(existingRow);
                            if (existingRowCellsUpdated.Count == 1 && columnIndex < Helper.GetColumnIndexByCellName(existingRowCellsUpdated[0].CellReference.Value))
                            {
                                Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                                existingRowCellsUpdated[0].InsertBeforeSelf(cell);

                                CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                                this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                            }
                            else if (existingRowCellsUpdated.Count == 1 && columnIndex > Helper.GetColumnIndexByCellName(existingRowCellsUpdated[0].CellReference.Value))
                            {
                                Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                                existingRowCellsUpdated[0].InsertAfterSelf(cell);

                                CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                                this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                            }
                            else
                            {
                                for (int i = 0; i < columnCount; i++)
                                {
                                    if (i >= existingRowCellsUpdated.Count)
                                    {
                                        Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                                        existingRow.AppendChild(cell);

                                        CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                                        this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                                        break;
                                    }
                                    else if (columnIndex < Helper.GetColumnIndexByCellName(existingRowCellsUpdated[i].CellReference.Value))
                                    {
                                        Cell cell = new Cell() { CellReference = Helper.GetCellName((int)rowIndex, columnIndex) };
                                        existingRowCellsUpdated[i].InsertBeforeSelf(cell);

                                        CellData cellData = new CellData(cell.CellReference.Value, false, worksheetName);
                                        this._cellReferencesWithWorksheetNameAndCells[cellData.CellReferenceWithSheetName] = cell;
                                        break;
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        public double GetColumnWidth(string cellValue)
        {
            if (String.IsNullOrWhiteSpace(cellValue))
            {
                return 0;
            }

            double maximumDigitWidh = 6D;
            //double width = Math.Truncate((cellValue.Length * (maximumDigitWidh + 5.0)) / maximumDigitWidh * 200) / 256;
            double width = (maximumDigitWidh * cellValue.Length / 7D * 256 - 128 / 7) / 256;
            width = Math.Round(width + 0.2D, 2);

            return width;
        }

        public int InsertSharedStringItem(string xmlContent)
        {
            SharedStringTablePart shareStringPart = this.GetSharedStringPart();
            // If the part does not contain a SharedStringTable, create one.
            if (shareStringPart.SharedStringTable == null)
            {
                shareStringPart.SharedStringTable = new SharedStringTable();
            }

            int i = 0;

            // Iterate through all the items in the SharedStringTable. If the text already exists, return its index.
            foreach (SharedStringItem item in shareStringPart.SharedStringTable.Elements<SharedStringItem>())
            {
                if (item.InnerXml?.Equals(xmlContent) ?? false)
                {
                    return i;
                }

                i++;
            }

            // The text does not exist in the part. Create the SharedStringItem and return its index.
            //shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(new DocumentFormat.OpenXml.Spreadsheet.Text(text)));
            try
            {
                shareStringPart.SharedStringTable.AppendChild(new SharedStringItem(xmlContent));
                shareStringPart.SharedStringTable.Save();
            }
            catch (Exception exception)
            {
                return -1;
            }

            return i;
        }

        private SharedStringTablePart GetSharedStringPart()
        {
            SharedStringTablePart shareStringPart;
            if (this._doc.WorkbookPart.GetPartsOfType<SharedStringTablePart>().Count() > 0)
            {
                shareStringPart = this._doc.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
            }
            else
            {
                shareStringPart = this._doc.WorkbookPart.AddNewPart<SharedStringTablePart>();
            }

            return shareStringPart;
        }

        public SharedStringItem GetSharedStringItem(int index)
        {
            SharedStringTablePart sharedStringTablePart =  this.GetSharedStringPart();
            SharedStringItem sharedStringItem = sharedStringTablePart.SharedStringTable.ElementAt(index) as SharedStringItem;
            return sharedStringItem;
        }

        public void Dispose()
        {
            this._doc.Dispose();
            this._doc = null;
        }
    }
}
