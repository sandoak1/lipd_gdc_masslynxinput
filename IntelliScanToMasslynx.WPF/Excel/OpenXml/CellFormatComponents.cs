﻿using DocumentFormat.OpenXml.Spreadsheet;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class CellFormatComponents
    {
        public Alignment Alignment { get; set; }

        public Font Font { get; set; }

        public Border Border { get; set; }

        public Fill Fill { get; set; }

        public uint NumberFormatId { get; set; }

        public string NumberingFormatCode { get; set; }
    }
}