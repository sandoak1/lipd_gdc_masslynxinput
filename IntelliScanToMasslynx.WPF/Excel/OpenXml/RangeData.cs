﻿using System;
using System.Text.RegularExpressions;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class RangeData
    {
        private string _sheetName;
        private CellData _rangeStart;
        private CellData _rangeEnd;

        public string SheetName
        {
            get
            {
                return this._sheetName;
            }
        }

        public CellData RangeStart
        {
            get
            {
                return this._rangeStart;
            }
        }

        public CellData RangeEnd
        {
            get
            {
                return this._rangeEnd;
            }
        }

        public string Range
        {
            get
            {
                string sheetName = String.IsNullOrWhiteSpace(this._sheetName) ? String.Empty : String.Concat(this.SheetName, "!");
                return String.Format("{0}{1}:{2}", sheetName, this.RangeStart.CellReference, this.RangeEnd.CellReference);
            }
        }

        public RangeData(string sheetName, CellData rangeStart, CellData rangeEnd)
        {
            this._sheetName = sheetName;
            this._rangeStart = rangeStart;
            this._rangeEnd = rangeEnd;
        }

        public static bool TryParse(string text, out RangeData rangeData)
        {
            rangeData = null;
            string[] splittedText = text.Replace("$", String.Empty).Split(new string[] { "!", ":" }, StringSplitOptions.RemoveEmptyEntries);
            if (!(splittedText.Length == 2 || splittedText.Length == 3))
            {
                return false;
            }

            if (splittedText.Length == 2)
            {
                Regex regex = new Regex(@"^[A-Z]{1,3}\d{1,7}$");
                if (!regex.IsMatch(splittedText[0]) || !regex.IsMatch(splittedText[1]))
                {
                    return false;
                }

                rangeData = new RangeData(String.Empty, new CellData(splittedText[0], false), new CellData(splittedText[1], false));
            }
            else if (splittedText.Length == 3)
            {
                Regex regex = new Regex(@"^[A-Z]{1,3}\d{1,7}$");
                if (!regex.IsMatch(splittedText[1]) || !regex.IsMatch(splittedText[2]))
                {
                    return false;
                }

                rangeData = new RangeData(splittedText[0], new CellData(splittedText[1], false), new CellData(splittedText[2], false));
            }

            return true;
        }
    }
}