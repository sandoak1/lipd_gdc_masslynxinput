﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public static class CellFormatHelper
    {
        public const string DEFAULT_FONT_NAME = "Arial";
        public const double DEFAULT_FONT_SIZE = 11D;

        private const string GENERAL_INTELLISCAN_FILE_GRREN_FILL_COLOR_HEX = "FF7FFFD4";
        private const string GENERAL_INTELLISCAN_FILE_GRREN_FONT_COLOR_HEX = "FF006100";
        public const string GENERAL_INTELLISCAN_FILE = "GeneralIntelliScanFile";
        public const string GENERAL_INTELLISCAN_FILE_GREEN_FILL = "GeneralIntelliScanGreenFill";

        public const string GENERAL_PHYSCHEM_FILE = "GeneralPhysChemFile";

        private static Dictionary<string, CellFormatComponents> _cellFormats = new Dictionary<string, CellFormatComponents>();

        static CellFormatHelper()
        {
            _cellFormats.Add(GENERAL_INTELLISCAN_FILE, new CellFormatComponents
            {
                Font = new Font
                {
                    FontSize = new FontSize { Val = 10D },
                    FontName = new FontName { Val = "Arial" },
                },
            });

            _cellFormats.Add(GENERAL_INTELLISCAN_FILE_GREEN_FILL, new CellFormatComponents
            {
                Font = new Font
                {
                    FontSize = new FontSize { Val = 10D },
                    FontName = new FontName { Val = "Arial" },
                },
                Fill = new Fill
                {
                    PatternFill = new PatternFill
                    {
                        PatternType = PatternValues.Solid,
                        ForegroundColor = new ForegroundColor { Rgb = GENERAL_INTELLISCAN_FILE_GRREN_FILL_COLOR_HEX },
                    }
                },
            });

            #region PhysChemFile


            _cellFormats.Add(GENERAL_PHYSCHEM_FILE, new CellFormatComponents
            {
                Font = new Font
                {
                    FontSize = new FontSize { Val = 11D },
                    FontName = new FontName { Val = "Arial" },
                },
            });

            #endregion PhysChemFile
        }

        public static CellFormatComponents GetCellFormatComponentsByName(string name)
        {
            if (!_cellFormats.ContainsKey(name))
            {
                throw new Exception("There is no cell fomrmat componenets with the given key.");
            }

            return _cellFormats[name];
        }
    }
}
