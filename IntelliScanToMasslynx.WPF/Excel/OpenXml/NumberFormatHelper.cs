﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public static class NumberFormatHelper
    {
        public static uint NumberingFormatIdsFromOldReport { get; set; } = 800;

        public const uint NUMBERFORMATID = 700;
        public const uint DATEFORMATID = 701;
        public static uint GENERAL_NUMBERFORMAT_CUSTOM { get; set; } = 702U;
        public static uint GENERAL_NUMBERFORMAT_CUSTOMDATE { get; set; } = 703U;
    }
}
