﻿using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class CellValuePatient
    {
        public string Value { get; set; }

        public string OuterXml { get; set; }

        public CellValues CellValues { get; set; }
    }
}
