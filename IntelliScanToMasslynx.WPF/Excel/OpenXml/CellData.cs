﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class CellData
    {
        private const string DOLLAR = "$";

        private int _rowIndex;
        private int _columnIndex;
        private bool _isDollar;
        private bool _isWithSheetName;

        /// <summary>
        /// Gets the index of the row.
        /// </summary>
        public int RowIndex
        {
            get
            {
                return this._rowIndex;
            }
        }

        /// <summary>
        /// Gets the index of the column.
        /// </summary>
        public int ColumnIndex
        {
            get
            {
                return this._columnIndex;
            }
        }

        /// <summary>
        /// Gets the name of the column ('A', 'ML', 'EVO', 'IX').
        /// </summary>
        public string ColumnName
        {
            get
            {
                return Helper.GetColumnNameByColumnIndex(this._columnIndex);
            }
        }

        /// <summary>
        /// Gets the cellreference ('A1', 'E777', 'BF57')
        /// </summary>
        public string CellReferenceWithSheetName
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(this.WorksheetName))
                {
                    return String.Concat(this.WorksheetName, "!", this.CellReference);
                }

                return this.CellReference;
            }
        }

        public string CellReference
        {
            get
            {
                if (this._isDollar)
                {
                    return String.Concat(DOLLAR, this.ColumnName, DOLLAR, this.RowIndex);
                }

                return String.Concat(this.ColumnName, this.RowIndex);
            }
        }

        /// <summary>
        /// Gets the worksheet name where the cell is.
        /// </summary>
        public string WorksheetName { get; }

        public CellData(int rowIndex, int columnIndex, bool isDollar, string worksheetName = null)
        {
            this._rowIndex = rowIndex;
            this._columnIndex = columnIndex;
            this._isDollar = isDollar;
            this.WorksheetName = worksheetName;
        }

        public CellData(int rowIndex, string columnName, bool isDollar, string worksheetName = null)
        {
            this._rowIndex = rowIndex;
            this._columnIndex = Helper.GetColumnIndexByColumnName(columnName);
            this._isDollar = isDollar;
            this.WorksheetName = worksheetName;
        }

        public CellData(string cellReference, bool isDollar, string worksheetName = null)
        {
            this._rowIndex = (int)Helper.GetRowIndex(cellReference);
            this._columnIndex = Helper.GetColumnIndexByColumnName(Helper.GetColumnName(cellReference));
            this._isDollar = isDollar;
            this.WorksheetName = worksheetName;
        }

        public CellData ShiftRow(int number)
        {
            if (this._rowIndex + number < 1)
            {
                throw new Exception("The new row index cannot be less than 1.");
            }

            return new CellData(this._rowIndex + number, this._columnIndex, this._isDollar, this.WorksheetName);
        }

        public CellData ShiftColumn(int number)
        {
            if (this._columnIndex + number < 1)
            {
                throw new Exception("The new column index cannot be less than 1.");
            }

            return new CellData(this._rowIndex, this._columnIndex + number, this._isDollar, this.WorksheetName);
        }
    }
}
