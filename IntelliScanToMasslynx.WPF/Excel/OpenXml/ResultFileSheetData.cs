﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class ResultFileSheetData
    {
        public string SheetName { get; set; }

        public int RowCounter { get; set; }

        public int ColumnCount { get; set; }

        public double ClinicalTrialCodeColumnWidth { get; set; }

        public double StudyLeadColumnWidth { get; set; }

        public double MedicalLeadColumnWidth { get; set; }

        //public IList<string> ColumnsShouldAddAfterUserDefinedColumns = new List<string>();
    }
}
