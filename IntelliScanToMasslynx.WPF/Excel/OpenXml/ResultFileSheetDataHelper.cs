﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Excel.OpenXml
{
    public class ResultFileSheetDataHelper
    {
        private List<ResultFileSheetData> _resultFileSheetData = new List<ResultFileSheetData>();

        public static Lazy<ResultFileSheetDataHelper> Instance { get; } = new Lazy<ResultFileSheetDataHelper>(() => new ResultFileSheetDataHelper());

        public ResultFileSheetData this[string sheetName]
        {
            get
            {
                IList<ResultFileSheetData> result = Instance.Value._resultFileSheetData.Where(x => x.SheetName.Equals(sheetName)).ToList();
                if (result == null || result.Count == 0)
                {
                    throw new Exception($"There is no data with the key '{sheetName}'");
                }
                if (result.Count > 1)
                {
                    throw new Exception($"There is more than one result with the key '{sheetName}'");
                }

                return result.First();
            }

            set
            {
                if (value == null)
                {
                    throw new Exception("The value cannot be null.");
                }

                if (this._resultFileSheetData.Any(x => x.SheetName.Equals(value.SheetName)))
                {
                    throw new Exception("This data is already added.");
                }

                this._resultFileSheetData.Add(value);
            }
        }
    }
}
