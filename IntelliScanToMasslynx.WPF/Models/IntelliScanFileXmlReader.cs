﻿using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class IntelliScanFileXmlReader
    {
        private string _path;
        private const string WELL_LOCATION_COLUMN_NAME = "WELL LOCATION";
        private const string COMPOUND_COLUMN_NAME = "COMPOUND";
        private const string USER_NAME_COLUMN_NAME = "USERNAME";
        private const string PROTOCOL_NAME_COLUMN_NAME = "PROTOCOL_NAME";
        private const string EXACT_MASS_COLUMN_NAME = "Exact Mass";
        private const string SHEET_NAME = "Order Details";
        private const string WORKSHEET = "Worksheet";
        private const string ROW = "Row";
        private const string CELL = "Cell";
        private const string BLANK = "BLANK";

        public IntelliScanFileXmlReader(string path)
        {
            if (String.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentNullException(nameof(path));
            }
            if (!path.ToLower().EndsWith(".xml"))
            {
                throw new ArgumentException("The given IntelliScan file is invalid. It is not .xml file.");
            }
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("The given IntelliScan file does not exist.");
            }

            this._path = path;
        }

        public IList<IntelliScanFileModel> Read()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started parsing the IntelliScan file.");

            XDocument doc = XDocument.Load(this._path);
            XElement worksheetElement = doc.Root.Descendants().SingleOrDefault(element =>
            {
                bool isWorksheet = element.Name.LocalName.Equals(WORKSHEET)
                    && element.Attributes().FirstOrDefault(x => x.Name.LocalName.Equals("Name")) != null
                && element.Attributes().FirstOrDefault(x => x.Name.LocalName.Equals("Name")).Value.Equals(SHEET_NAME);
                return isWorksheet;
            });
            if (worksheetElement == null)
            {
                throw new PLAErrorException("The given IntelliScan file is invalid.", null);
            }

            IList<IntelliScanFileModel> intelliScanFileModels = new List<IntelliScanFileModel>();
            var rows = worksheetElement.Descendants().Where(x => x.Name.LocalName.Equals(ROW));
            int rowIndex = 0;
            int wellLocationColummnIndex = -1;
            int compoundColummnIndex = -1;
            int userNameColummnIndex = -1;
            int protocolNameColummnIndex = -1;
            int exactMassColummnIndex = -1;
            int parsedCorrect = 0;
            int parsedInCorrect = 0;

            foreach (var row in rows)
            {
                try
                {
                    int columnIndex = 0;
                    IntelliScanFileModel intelliScanFileModel = new IntelliScanFileModel();
                    foreach (var cell in row.Descendants().Where(x => x.Name.LocalName.Equals(CELL)))
                    {
                        if (rowIndex == 0)
                        {
                            if (cell.Value.Equals(WELL_LOCATION_COLUMN_NAME))
                            {
                                wellLocationColummnIndex = columnIndex;
                            }
                            if (cell.Value.Equals(COMPOUND_COLUMN_NAME))
                            {
                                compoundColummnIndex = columnIndex;
                            }
                            if (cell.Value.Equals(USER_NAME_COLUMN_NAME))
                            {
                                userNameColummnIndex = columnIndex;
                            }
                            if (cell.Value.Equals(PROTOCOL_NAME_COLUMN_NAME))
                            {
                                protocolNameColummnIndex = columnIndex;
                            }
                            if (cell.Value.Equals(EXACT_MASS_COLUMN_NAME))
                            {
                                exactMassColummnIndex = columnIndex;
                            }
                            columnIndex++;
                            continue;
                        }

                        if (rowIndex == 1 && columnIndex == 0)
                        {
                            if (wellLocationColummnIndex == -1)
                            {
                                throw new PLAErrorException("The given IntelliScan file is not vald. The column 'well location' is missing.", null);
                            }
                            if (compoundColummnIndex == -1)
                            {
                                throw new PLAErrorException("The given IntelliScan file is not vald. The column 'compound' is missing.", null);
                            }
                            if (userNameColummnIndex == -1)
                            {
                                throw new PLAErrorException("The given IntelliScan file is not vald. The column 'user name' is missing.", null);
                            }
                            if (protocolNameColummnIndex == -1)
                            {
                                throw new PLAErrorException("The given IntelliScan file is not vald. The column 'protocol name' is missing.", null);
                            }
                            if (exactMassColummnIndex == -1)
                            {
                                throw new PLAErrorException("The given IntelliScan file is not vald. The column 'exact mass' is missing.", null);
                            }
                        }

                        double parsed = -1D;
                        if (columnIndex == wellLocationColummnIndex)
                        {
                            intelliScanFileModel.WellLocation = cell.Value;
                        }
                        if (columnIndex == compoundColummnIndex)
                        {
                            intelliScanFileModel.Compound = cell.Value;
                        }
                        if (columnIndex == userNameColummnIndex)
                        {
                            intelliScanFileModel.UserName = cell.Value;
                        }
                        if (columnIndex == protocolNameColummnIndex)
                        {
                            intelliScanFileModel.ProtocolName = cell.Value;
                        }
                        if (columnIndex == exactMassColummnIndex)
                        {
                            if (Double.TryParse(cell.Value, out parsed))
                            {
                                intelliScanFileModel.ExactMass = parsed;
                            }
                        }

                        columnIndex++;
                    }

                    if (rowIndex != 0 && !IntelliScanFileModel.IsNullOrEmthy(intelliScanFileModel) && !intelliScanFileModel.Compound.ToUpper().Equals(BLANK))
                    {
                        intelliScanFileModel.RowNumber = (uint)rowIndex + 2U;//header, rowIndex is zero based
                        intelliScanFileModels.Add(intelliScanFileModel);
                    }
                    parsedCorrect++;
                }
                catch (Exception exception)
                {
                    parsedInCorrect++;
                    Utilities.Instance.Log(LogLevels.Error, $"The #{rowIndex + 2} line is invalid.", exception);
                }
                rowIndex++;
            }

            Utilities.Instance.Log(LogLevels.Information, $"#{parsedCorrect} line{(parsedCorrect == 1 ? "": "s")} {(parsedCorrect == 1 ? "has" : "have")} been parsed successfully.");
            Utilities.Instance.Log(LogLevels.Information, $"#{parsedInCorrect} line{(parsedCorrect == 1 ? "": "s")} {(parsedCorrect == 1 ? "was" : "were")} not parsed successfully.");
            Utilities.Instance.Log(LogLevels.Information, "Finished parsing the IntelliScan file.");
            
            return intelliScanFileModels;
        }
    }
}
