﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class HeaderColumn
    {
        public HeaderColumn(IList<Column> titles)
        {
            if (titles == null)
            {
                throw new ArgumentNullException(nameof(titles));
            }
            if (titles.Count == 0)
            {
                throw new ArgumentException(nameof(titles));
            }
            if (titles.Count(x => !x.IsRemanedName) != 1)
            {
                throw new ArgumentException("Only one title can be used. THe others should be old, renamed title.", nameof(titles));
            }

            this.Titles = titles;
        }

        public IList<Column> Titles { get; set; } = new List<Column>();

        public IList<ColumnName> ColumnNameInExcel { get; set; } = new List<ColumnName>();

        public string SheetName { get; set; }

        public string ModelClassPropertyName { get; set; }

        public bool IsLastColumn { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// True: If the excel contains this column
        /// </summary>
        public bool IsUsed { get; set; }

        public static HeaderColumn GetLastColumnName(IList<HeaderColumn> headerColumns)
        {
            if (headerColumns == null || headerColumns.Count == 0)
            {
                throw new ArgumentNullException(nameof(headerColumns));
            }

            IList<HeaderColumn> lasts = headerColumns.Where(x => x.IsLastColumn).ToList();
            if (lasts.Count > 1)
            {
                throw new Exception("There are more than one last column.");
            }

            return lasts[0];
        }
    }
}
