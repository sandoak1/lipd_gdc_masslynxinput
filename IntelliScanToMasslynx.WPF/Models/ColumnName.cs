﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class ColumnName
    {
        public string SheetName { get; set; }

        public string ColumnNameTitle { get; set; }
    }
}
