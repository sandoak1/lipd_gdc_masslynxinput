﻿using DocumentFormat.OpenXml.Spreadsheet;
using IntelliScanToMasslynx.WPF.Excel.OpenXml;
using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class IntelliScanFileReader : IDisposable
    {
        public class ReadResult
        {
            public string WorksheetName { get; set; }

            public int IncorrectRowCount { get; set; }

            public int CorrectRowCount { get; set; }
        }

        private string _filePath;
        private ExcelManager _excelManager;
        private IList<HeaderColumn> _headerColumns = new List<HeaderColumn>();
        private CancellationTokenSource _cancellationTokenSource;

        public IList<ReadResult> ReadResults { get; } = new List<ReadResult>();

        public IList<HeaderColumn> HeaderColumns { get { return this._headerColumns; } }


        public IntelliScanFileReader(string filePath, CancellationTokenSource cancelltationTokenSource)
        {
            if (String.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException(nameof(filePath), "The input excel file path cannot be null or empty.");
            }
            if (!Path.GetExtension(filePath).Equals(".xlsx"))
            {
                throw new ArgumentException($"The input file is not an excel file. The path: '{filePath}'", nameof(filePath));
            }
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("The given input file does not exists.", filePath);
            }
            if (cancelltationTokenSource == null)
            {
                throw new ArgumentNullException(nameof(cancelltationTokenSource));
            }

            this._filePath = filePath;
            this._excelManager = new ExcelManager(filePath);
            this._cancellationTokenSource = cancelltationTokenSource;

            IList<string> worksheetNames = this._excelManager.GetWorksheetNames();

            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "WELL LOCATION", IsRemanedName = false } })) { IsRequired = true, ModelClassPropertyName = nameof(IntelliScanFileModel.WellLocation) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "COMPOUND", IsRemanedName = false } })) { IsRequired = true, ModelClassPropertyName = nameof(IntelliScanFileModel.Compound) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "USERNAME", IsRemanedName = false } })) { IsRequired = true, ModelClassPropertyName = nameof(IntelliScanFileModel.UserName) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "PROTOCOL_NAME", IsRemanedName = false } })) { IsRequired = true, ModelClassPropertyName = nameof(IntelliScanFileModel.ProtocolName) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Exact Mass", IsRemanedName = false } })) { IsRequired = true, ModelClassPropertyName = nameof(IntelliScanFileModel.ExactMass) });
        }

        public IList<IntelliScanFileModel> ReadData()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started parsing the IntelliScan file.");

            IList<string> sheetNames = this._excelManager.GetWorksheetNames();

            if (sheetNames?.Count == 0)
            {
                throw new PLAErrorException($"The IntelliScan file has no sheet.", null);
            }
            string sheetNameToRead = sheetNames[0];

            int rowNumber = 0;
            int correntRowCount = 0;
            int incorrentRowCount = 0;
            IEnumerable<Row> rows = null;
            List<IntelliScanFileModel> intelliScanFileModels = new List<IntelliScanFileModel>();
            IList<HeaderColumn> currentHeaders = null;

            try
            {
                SheetData sheetData = this._excelManager.GetSheetData(sheetNameToRead);

                rows = this._excelManager.GetRows(sheetNameToRead);
            }
            catch (Exception exception)
            {
                Utilities.Instance.Log(LogLevels.Error, $"Error occured during reading the sheet: '{sheetNameToRead}'", exception);
            }

            foreach (Row row in rows)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return null;
                }

                rowNumber++;

                if (rowNumber == 1)
                {
                    currentHeaders = ValidateHeaderRow(row, sheetNameToRead).ToList();
                    continue;
                }

                IList<Cell> rowCells = this._excelManager.GetRowCells(row);

                try
                {
                    IntelliScanFileModel intelliScanFileModelToAdd = new IntelliScanFileModel();

                    for (int i = 0; i < currentHeaders.Count; i++)
                    {
                        if (!currentHeaders[i].IsRequired && !currentHeaders[i].IsUsed)
                        {
                            continue;
                        }

                        string propertyName = currentHeaders[i].ModelClassPropertyName;
                        string cellName = $"{currentHeaders[i].ColumnNameInExcel.SingleOrDefault(x => x.SheetName.Equals(sheetNameToRead)).ColumnNameTitle}{row.RowIndex.InnerText}";
                        Cell cell = rowCells.SingleOrDefault(x => x.CellReference.Value.Equals(cellName));
                        CellValuePatient cellValue = this._excelManager.GetCellValue(cell);
                        this.SetProperty(intelliScanFileModelToAdd, propertyName, cellValue, (int)row.RowIndex.Value);
                    }

                    if (IntelliScanFileModel.IsNullOrEmthy(intelliScanFileModelToAdd))
                    {
                        continue;
                    }

                    intelliScanFileModelToAdd.RowNumber = row.RowIndex.Value;
                    intelliScanFileModels.Add(intelliScanFileModelToAdd);
                    correntRowCount++;
                }
                catch (Exception exception)
                {
                    incorrentRowCount++;
                    Utilities.Instance.Log(LogLevels.Error, $"Error occued parsing the {rowNumber}. line from the sheet '{sheetNameToRead}' from the IntelliScan file.", exception);
                }
            }

            this.ReadResults.Add(new ReadResult { WorksheetName = sheetNameToRead, IncorrectRowCount = incorrentRowCount, CorrectRowCount = correntRowCount });

            Utilities.Instance.Log(LogLevels.Information, $"{correntRowCount} lines have been parsed from the sheet '{sheetNameToRead}'.");
            Utilities.Instance.Log(LogLevels.Warning, $"{incorrentRowCount} lines could not be parsed from the sheet '{sheetNameToRead}'.");
            Utilities.Instance.Log(LogLevels.Information, "Finished parsing the IntelliScan file.");

            return intelliScanFileModels;
        }
        
        private IEnumerable<HeaderColumn> ValidateHeaderRow(Row headerRow, string worksheetName)
        {
            if (String.IsNullOrWhiteSpace(worksheetName))
            {
                throw new ArgumentNullException(nameof(worksheetName));
            }
            if (headerRow.RowIndex.Value != 1)
            {
                throw new ArgumentException("The parameter is not the header row.", nameof(headerRow));
            }

            IList<Cell> headerCells = this._excelManager.GetRowCells(headerRow);
            IList<CellValuePatient> headerCellValues = this._excelManager.GetRowCellValues(headerRow);

            foreach (HeaderColumn headerColumn in this._headerColumns)
            {
                IList<string> titlesToUpper = headerColumn.Titles.Select(x => x.Name.ToUpper()).ToList();
                Cell actualCell = headerCells.SingleOrDefault(x => titlesToUpper.Contains(this._excelManager.GetCellValue(x).Value.ToUpper()));
                if (actualCell == null && headerColumn.IsRequired)
                {
                    throw new PLAWarningException($"Invalid IntelliScan file. Please give a valid IntelliScan file. The column named '{headerColumn.Titles.First(x => !x.IsRemanedName).Name}' is missing from the worksheet named '{worksheetName}'.", null);
                }
                if (actualCell == null)
                {
                    continue;
                }

                string columnName = Helper.GetColumnName(actualCell.CellReference.Value);
                headerColumn.ColumnNameInExcel.Add(new ColumnName { ColumnNameTitle = columnName, SheetName = worksheetName });
            }

            IList<string> optionalHeaderTitles = this._headerColumns.Where(x => !x.IsRequired).Select(x => x.Titles.Select(y => y.Name.ToUpper())).SelectMany(x => x).ToList();
            IList<string> optionalHeaderTitlesFromTheFiles = headerCells.Select(x => this._excelManager.GetCellValue(x)).Select(x => x.Value.ToUpper()).Where(x => optionalHeaderTitles.Contains(x)).ToList();
            if (optionalHeaderTitlesFromTheFiles.Count > 0)
            {
                var usedOptionalHeaderTitleInTheFiles = optionalHeaderTitles.Where(x => optionalHeaderTitlesFromTheFiles.Select(y => y.ToUpper()).Contains(x.ToUpper())).ToList();

                var usedColumns = this._headerColumns.Where(x => x.IsRequired
                    || (!x.IsRequired && x.Titles.Select(y => y.Name.ToUpper()).Any(z => usedOptionalHeaderTitleInTheFiles.Contains(z)))
                ).ToList();

                foreach (HeaderColumn item in usedColumns.Where(x => !x.IsRequired))
                {
                    item.IsUsed = true;
                }
                return usedColumns;
            }

            return this._headerColumns.Where(x => x.IsRequired).ToList();
        }

        private void SetProperty(IntelliScanFileModel intelliScanFileModel, string propertyName, CellValuePatient value, int rowNumber)
        {
            if (value == null)
            {
                value = new CellValuePatient();
            }
            int parsedNumber = -1;
            double parseNumberDouble = -1D;
            DateTime parsedDatetime = DateTime.MinValue;

            PropertyInfo propertyToSet = intelliScanFileModel.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);

            if (propertyToSet.PropertyType.Equals(typeof(string)))
            {
                if (value.CellValues == CellValues.SharedString)
                {
                    propertyToSet.SetValue(intelliScanFileModel, value.Value, null);
                    PropertyInfo propertyToSetSharedString = intelliScanFileModel.GetType().GetProperty($"{propertyName}{IntelliScanFileModel.SharedStringPropertyNamePostFix}", BindingFlags.Public | BindingFlags.Instance);
                    propertyToSetSharedString?.SetValue(intelliScanFileModel, value.OuterXml, null);
                }
                else
                {
                    propertyToSet.SetValue(intelliScanFileModel, value.Value, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(int?)))
            {
                if (Int32.TryParse(value.Value, out parsedNumber))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedNumber, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(int)))
            {
                if (Int32.TryParse(value.Value, out parsedNumber))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedNumber, null);
                }
                else
                {
                    string message = $"The IntellyScan file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message);
                    throw new Exception(message);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(double?)))
            {
                if (Double.TryParse(value.Value, out parseNumberDouble))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parseNumberDouble, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(double)))
            {
                if (Double.TryParse(value.Value, out parseNumberDouble))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parseNumberDouble, null);
                }
                else
                {
                    string message = $"The IntellyScan file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message);
                    throw new Exception(message);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(DateTime?)))
            {
                if (DateTime.TryParse(value.Value, out parsedDatetime))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedDatetime, null);
                }
                else
                {
                    if (Double.TryParse(value.Value, out parseNumberDouble))
                    {
                        propertyToSet.SetValue(intelliScanFileModel, DateTime.FromOADate(parseNumberDouble), null);
                    }
                }
                //propertyToSet.SetValue(studyItem, null, null);
            }
            else if (propertyToSet.PropertyType.Equals(typeof(bool?)))
            {
                bool? valueAsBool = String.IsNullOrWhiteSpace(value.Value) ? default(bool?) : (value.Value.ToUpper().Equals("Y") || value.Value.ToUpper().Equals("YES") || value.Value.ToUpper().Equals("TRUE")) ? true : false;
                propertyToSet.SetValue(intelliScanFileModel, valueAsBool, null);
            }
            else if (propertyToSet.PropertyType.Equals(typeof(bool)))
            {
                try
                {
                    bool valueAsBool = value.Value.ToUpper().Equals("Y") ? true : false;
                    propertyToSet.SetValue(intelliScanFileModel, valueAsBool, null);
                }
                catch (Exception exception)
                {
                    string message = $"The IntellyScan file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message, exception);
                    propertyToSet.SetValue(intelliScanFileModel, false, null);
                }
            }
        }

        public void Dispose()
        {
            this._excelManager.Dispose();
            this._excelManager = null;
        }
    }
}
