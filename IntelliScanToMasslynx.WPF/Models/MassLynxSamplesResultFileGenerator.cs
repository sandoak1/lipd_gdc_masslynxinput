﻿using DocumentFormat.OpenXml.Spreadsheet;
using IntelliScanToMasslynx.WPF.Comparers;
using IntelliScanToMasslynx.WPF.Converters;
using IntelliScanToMasslynx.WPF.Excel.OpenXml;
using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class MassLynxSamplesResultFileGenerator : IDisposable
    {
        public IList<MassLynxSamplesFileWorklistSheetModel> MassLynxSamplesFileWorklistSheetModels { get; private set; }

        public IList<IntelliScanFileModel> IntelliScanFileModelsRemovedDuplications { get; private set; }

        private CancellationTokenSource _cancellationTokenSource;
        private string _documentPath;
        private string _duplicatedSheetName;
        private ExcelManager _excelManager;
        private string _templatePath;
        private string _volumeSheetName;
        private string _workListSheetName;
        private IList<IntelliScanFileModel> _intelliScanFileData;
        private IList<IntelliScanFileModel> _intelliScanFileDataOrdered;
        private ConfigurationFileModel _configuration;
        private string _userName;
        private uint _userGivenFileNameCount;

        public MassLynxSamplesResultFileGenerator(string documentPath, string templatePath, IList<IntelliScanFileModel> intelliScanFileData, IList<IntelliScanFileModel> intelliScanFileDataOrdered, ConfigurationFileModel configuration, string userName, uint userGivenFileNameCount, CancellationTokenSource cancellationTokenSource)
        {
            if (String.IsNullOrWhiteSpace(documentPath))
            {
                throw new ArgumentNullException(nameof(documentPath), "The result MassLynxSampes excel file path cannot be null or empty.");
            }
            if (!Path.GetExtension(documentPath).ToLower().Equals(".xlsx"))
            {
                throw new ArgumentException($"The result MassLynxSampes file is not an excel file. The path: '{documentPath}'", nameof(documentPath));
            }
            if (String.IsNullOrWhiteSpace(templatePath))
            {
                throw new ArgumentNullException(nameof(templatePath), "The template excel file path cannot be null or empty.");
            }
            if (!Path.GetExtension(templatePath).ToLower().Equals(".xlsx"))
            {
                throw new ArgumentException($"The template file is not an excel file. The path: '{templatePath}'", nameof(templatePath));
            }
            if (cancellationTokenSource == null)
            {
                throw new ArgumentNullException(nameof(cancellationTokenSource));
            }
            if (intelliScanFileData?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileData));
            }
            if (intelliScanFileDataOrdered?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileDataOrdered));
            }
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            if (configuration.Parameters?.Count == 0 || configuration.Standards?.Count == 0 || configuration.Volumes?.Count == 0)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            if (String.IsNullOrWhiteSpace(userName))
            {
                throw new ArgumentNullException(nameof(userName));
            }

            this._documentPath = documentPath;
            this._templatePath = templatePath;
            this._cancellationTokenSource = cancellationTokenSource;
            this._excelManager = new ExcelManager(this._documentPath, this._templatePath, true);
            this._intelliScanFileData = intelliScanFileData;
            this._intelliScanFileDataOrdered = intelliScanFileDataOrdered;
            this._configuration = configuration;
            this._userName = userName;
            this._userGivenFileNameCount = userGivenFileNameCount;

            this._workListSheetName = this._excelManager.GetWorksheetName(1);
            this._duplicatedSheetName = this._excelManager.GetWorksheetName(2);
            this._volumeSheetName = this._excelManager.GetWorksheetName(3);

            this._excelManager.GetWorkbook().CalculationProperties.ForceFullCalculation = true;
            this._excelManager.GetWorkbook().CalculationProperties.FullCalculationOnLoad = true;
        }

        public void Do()
        {
            #region Adding style formats

            this._excelManager.AddCellFormat(CellFormatHelper.GENERAL_INTELLISCAN_FILE, CellFormatHelper.GetCellFormatComponentsByName(CellFormatHelper.GENERAL_INTELLISCAN_FILE));
            this._excelManager.AddCellFormat(CellFormatHelper.GENERAL_INTELLISCAN_FILE_GREEN_FILL, CellFormatHelper.GetCellFormatComponentsByName(CellFormatHelper.GENERAL_INTELLISCAN_FILE_GREEN_FILL));

            #endregion Adding style formats
            this.GenerateDuplicatedSheet();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            this.MassLynxSamplesFileWorklistSheetModels = this.GenerateWorkListSheet();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }


            this.GenerateVolumeSheet(this.MassLynxSamplesFileWorklistSheetModels);
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }
        }
        
        private IList<MassLynxSamplesFileWorklistSheetModel> GenerateWorkListSheet()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the work list sheet.");

            List<IntelliScanFileModel> modelForWorkListsheet = this._intelliScanFileData.Where(x => !x.IsDuplicatedForDuplicatedSheet).ToList();

            IList<MassLynxSamplesFileWorklistSheetModel> data = new List<MassLynxSamplesFileWorklistSheetModel>();
            IList<MassLynxSamplesFileWorklistSheetModel> orderedData = new List<MassLynxSamplesFileWorklistSheetModel>();
            string sheetName = this._workListSheetName;
            Cell currentCell;

            foreach (ConfigurationFileModel.Standard configurationStandard in this._configuration.Standards)
            {
                MassLynxSamplesFileWorklistSheetModel massLynxSamplesFileWorklistSheetModel = new MassLynxSamplesFileWorklistSheetModel();
                massLynxSamplesFileWorklistSheetModel.Standard = configurationStandard;
                data.Add(massLynxSamplesFileWorklistSheetModel);
            }
            foreach (IntelliScanFileModel item in modelForWorkListsheet)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return null;
                }

                if (String.IsNullOrWhiteSpace(item.ProtocolName))
                {
                    Utilities.Instance.Log(LogLevels.Error, $"There is a model from the intelliScan file, whose protocol name is not defined.");
                    continue;
                }

                if (item.ProtocolName.ToUpper().Equals(Constants.PROTOCOL_NAME_FOR_ALL.ToUpper()) || item.IsDuplicatedForWorklistSheet)
                {
                    foreach (ConfigurationFileModel.Parameter configurationParameter in this._configuration.Parameters)
                    {
                        MassLynxSamplesFileWorklistSheetModel massLynxSamplesFileWorklistSheetModel = new MassLynxSamplesFileWorklistSheetModel();
                        massLynxSamplesFileWorklistSheetModel.IntelliScanFileModel = item;
                        massLynxSamplesFileWorklistSheetModel.Parameter = configurationParameter;
                        data.Add(massLynxSamplesFileWorklistSheetModel);
                    }
                }
                else
                {
                    var duplicatedPair = modelForWorkListsheet.Where(x => x.UserName.Equals(item.UserName) && x.Compound.Equals(item.Compound));
                    bool shouldAddToWorkList = !duplicatedPair.Select(x => x.ProtocolName).Contains(Constants.PROTOCOL_NAME_FOR_ALL);
                    if (shouldAddToWorkList)
                    {
                        ConfigurationFileModel.Parameter configurationParameter = this._configuration.Parameters.FirstOrDefault(x => x.Analysis.Equals(item.ProtocolName));
                        MassLynxSamplesFileWorklistSheetModel massLynxSamplesFileWorklistSheetModel = new MassLynxSamplesFileWorklistSheetModel();
                        massLynxSamplesFileWorklistSheetModel.IntelliScanFileModel = item;
                        massLynxSamplesFileWorklistSheetModel.Parameter = configurationParameter;
                        data.Add(massLynxSamplesFileWorklistSheetModel);
                    }
                }
            }

            int rowCounter = 1;//first row is the header
            string fileNameDate = DateTime.Now.ToString("dd-MMM-yyyy");
            uint fileNameCount = _userGivenFileNameCount > 0 ? _userGivenFileNameCount : 1;
            IEnumerable<IGrouping<string, MassLynxSamplesFileWorklistSheetModel>> orderedGroupedData = data.OrderBy(x => x, new DataComparerByFirstInletFile()).GroupBy(x => x.InletFile);

            foreach (IGrouping<string, MassLynxSamplesFileWorklistSheetModel> item in orderedGroupedData)
            {
                List<MassLynxSamplesFileWorklistSheetModel> orderedDataItems = item.OrderBy(x => x, new DataComparerByFirstBeforeAfterAndTaskName()).ToList();
                foreach (MassLynxSamplesFileWorklistSheetModel orderedDataItem in orderedDataItems)
                {
                    orderedData.Add(orderedDataItem);
                    rowCounter++;
                    int columnCounter = 1;

                    string sampleId = null;
                    string submitter = null;
                    string task = null;
                    string bottle = null;
                    string massA = null;
                    string massB = null;
                    string inletPrerum = null;
                    string inletFile = null;
                    string msFile = null;
                    string injectVolume = null;
                    string parameterFile = null;
                    string process = null;
                    if (orderedDataItem.Standard != null)
                    {
                        sampleId = orderedDataItem.Standard.SampleId;
                        bottle = orderedDataItem.Standard.Bottle;
                        massA = orderedDataItem.Standard.MassA;
                        massB = orderedDataItem.Standard.MassB;
                        inletPrerum = orderedDataItem.Standard.InletPrerun;
                        inletFile = orderedDataItem.Standard.InletFile;
                        msFile = orderedDataItem.Standard.MSFile;
                        try
                        {
                            var paramter = this._configuration.Parameters.FirstOrDefault(x => x.Analysis.Equals(orderedDataItem.Standard.Analysis));
                            injectVolume = paramter.InjectVolume.ToString();
                        }
                        catch (Exception exception)
                        {
                            Utilities.Instance.Log(LogLevels.Warning, $"Could not been determined the inject volume. Sample id: '{sampleId}'", exception);
                        }
                    }
                    else
                    {
                        sampleId = orderedDataItem.IntelliScanFileModel.Compound;
                        submitter = orderedDataItem.IntelliScanFileModel?.UserName?.ToUpper();
                        task = orderedDataItem.IntelliScanFileModel.IsDuplicatedForWorklistSheet ? Constants.PROTOCOL_NAME_FOR_ALL : orderedDataItem.IntelliScanFileModel?.ProtocolName;
                        try
                        {
                            bottle = WellPlateWellPositionTranslator.GetWellPosition(orderedDataItem.IntelliScanFileModel.WellLocation);
                        }
                        catch (Exception exception)
                        {
                            Utilities.Instance.Log(LogLevels.Warning, $"Could not been determined the bottle. Sample id: '{sampleId}'", exception);
                        }
                        massA = orderedDataItem.IntelliScanFileModel.ExactMass.HasValue ? orderedDataItem.IntelliScanFileModel.ExactMass.ToString() : "";
                        inletFile = orderedDataItem.Parameter.InletFile;
                        msFile = orderedDataItem.Parameter.MSFile;
                        injectVolume = orderedDataItem.Parameter.InjectVolume.ToString();
                        parameterFile = orderedDataItem.Parameter.ParameterFile;
                        process = orderedDataItem.Parameter.Process;
                    }
                    
                    string fileName = $"{fileNameDate}-{fileNameCount++.ToString("D3")}";
                    orderedDataItem.FileName = fileName;

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(this._userName.ToUpper()), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(fileName), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(sampleId), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(submitter), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(task), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(bottle), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(massA), DataType = CellValues.Number };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(massB), DataType = CellValues.Number };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(inletPrerum), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(inletFile), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(msFile), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(injectVolume), DataType = CellValues.Number };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(parameterFile), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                    currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(process), DataType = CellValues.String };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);
                }
            }
            
            Utilities.Instance.Log(LogLevels.Information, "Finished generating the work list sheet.");
            return orderedData;
        }

        private void GenerateDuplicatedSheet()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the duplicated sheet.");

            int rowCounter = 2;
            Cell currentCell = null;
            string sheetName = this._duplicatedSheetName;

            int duplicatedCommentColumn = 6;

            List<IntelliScanFileModel> insertedItems = new List<IntelliScanFileModel>();
            foreach (var item in this._intelliScanFileDataOrdered)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    break;
                }

                int columnCounter = 1;
                string bottle = null;
                try
                {
                    bottle = WellPlateWellPositionTranslator.GetWellPosition(item.WellLocation);
                }
                catch (Exception exception)
                {
                    Utilities.Instance.Log(LogLevels.Warning, $"Could not been determined the bottle. Sample id: '{item.Compound}'", exception);
                }

                item.RowNumberInTheResultFile = rowCounter;

                string format = CellFormatHelper.GENERAL_INTELLISCAN_FILE;
                if (insertedItems.Count(x => x.Compound.Equals(item.Compound) && x.UserName.Equals(item.UserName)) > 0)
                {
                    format = CellFormatHelper.GENERAL_INTELLISCAN_FILE_GREEN_FILL;
                    item.IsDuplicatedForDuplicatedSheet = true;
                }

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item?.Compound), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, format);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.WellLocation), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, format);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item?.UserName?.ToUpper()), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, format);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item?.ProtocolName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, format);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.ExactMass.HasValue ? item.ExactMass.ToString() : ""), DataType = CellValues.Number };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, format);

                if (item.IsDuplicatedForDuplicatedSheet)
                {
                    var referencedItem = insertedItems.FirstOrDefault(x => x.Compound.Equals(item.Compound) && x.UserName.Equals(item.UserName));
                    if (referencedItem != null)
                    {
                        referencedItem.IsDuplicatedForWorklistSheet = true;
                    }
                    string rowNumber = referencedItem == null ? "" : referencedItem.RowNumberInTheResultFile.ToString();

                    currentCell = new Cell()
                    {
                        CellReference = Helper.GetCellName(rowCounter, duplicatedCommentColumn),
                        DataType = CellValues.String,
                        CellValue = new CellValue($"Row has same value on [COMPOUND] as in row {rowNumber}. Will be deleted. Row {rowNumber} column PROTOCOL_NAME will be forced a value 'Trend Analysis All'")
                    };
                    this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                    this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);
                }
                
                rowCounter++;
                insertedItems.Add(item);
            }

            Utilities.Instance.Log(LogLevels.Information, "Finished generating the work list sheet.");
        }

        private void GenerateVolumeSheet(IList<MassLynxSamplesFileWorklistSheetModel> data)
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the volume sheet.");

            if (data?.Count == 0)
            {
                throw new ArgumentNullException(nameof(data));
            }

            string firstRunTimeCell = null;
            string lastRunTimeCell = null;
            string firstVolumeCell = null;
            string lastVolumeCell = null;
            string firstMsVolumenCell = null;
            string lastMsVolumeCell = null;

            int rowCounter = 2;
            Cell currentCell = null;
            string sheetName = this._volumeSheetName;
            var orderedData = data.GroupBy(x => x.InletFile).OrderByDescending(x => x.Count()).ToList();
            int columnCounter = 1;
            foreach (var orderedDataItem in orderedData)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return;
                }

                columnCounter = 1;
                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(orderedDataItem.Key), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

                var volume = this._configuration.Volumes.FirstOrDefault(x => x.Inlet.Equals(orderedDataItem.Key));
                string runTimeValue = null;
                string volumeValue = null;
                string msVolumeValue = null;

                if (volume == null)
                {
                    Utilities.Instance.Log(LogLevels.Warning, $"There is no volume for inlet '{orderedDataItem.Key}'");
                }
                else
                {
                    double runTime = orderedDataItem.Count() * volume.RunTime;
                    runTimeValue = runTime.ToString();
                    volumeValue = (runTime * volume.FlowRate).ToString();
                    msVolumeValue = (runTime * volume.MSFlowRate).ToString();
                }

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(runTimeValue), DataType = CellValues.Number };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);
                if (rowCounter == 2)
                {
                    firstRunTimeCell = currentCell.CellReference.Value;
                }
                lastRunTimeCell = currentCell.CellReference.Value;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(volumeValue), DataType = CellValues.Number };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);
                if (rowCounter == 2)
                {
                    firstVolumeCell = currentCell.CellReference.Value;
                }
                lastVolumeCell = currentCell.CellReference.Value;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(msVolumeValue), DataType = CellValues.Number };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);
                if (rowCounter == 2)
                {
                    firstMsVolumenCell = currentCell.CellReference.Value;
                }
                lastMsVolumeCell = currentCell.CellReference.Value;

                rowCounter++;
            }

            rowCounter++;
            columnCounter = 1;
            string formula = "=SUM(FIRST:LAST)";

            currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue("Total"), DataType = CellValues.String };
            this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
            this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

            currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellFormula = new CellFormula(formula.Replace("FIRST", firstRunTimeCell).Replace("LAST", lastRunTimeCell)) };
            this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
            this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

            currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellFormula = new CellFormula(formula.Replace("FIRST", firstVolumeCell).Replace("LAST", lastVolumeCell)) };
            this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
            this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

            currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellFormula = new CellFormula(formula.Replace("FIRST", firstMsVolumenCell).Replace("LAST", lastMsVolumeCell)) };
            this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
            this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_INTELLISCAN_FILE);

            Utilities.Instance.Log(LogLevels.Information, "Finished generating the volume sheet.");
        }

        public void Dispose()
        {
            this._excelManager.SaveWorksheet(this._workListSheetName);
            this._excelManager.SaveWorksheet(this._duplicatedSheetName);
            this._excelManager.SaveWorksheet(this._volumeSheetName);
            this._excelManager.Dispose();
            this._excelManager = null;
        }
    }
}
