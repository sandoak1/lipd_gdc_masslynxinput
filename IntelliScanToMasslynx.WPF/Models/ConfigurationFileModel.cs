﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    [Serializable]
    public class ConfigurationFileModel : ICloneable
    {
        [Serializable]
        public class BaseModel : BindableBase { }

        [Serializable]
        public class Parameter : BaseModel
        {
            private string _analysis;
            private string _inletFile;
            private string _MSFile;
            private double _iInjectVolume;
            private string _parameterFile;
            private string _process;

            public string Analysis
            {
                get { return _analysis; }
                set
                {
                    if (this._analysis != value)
                    {
                        base.SetProperty(ref this._analysis, value);
                    }
                }
            }

            public string InletFile
            {
                get { return _inletFile; }
                set
                {
                    if (this._inletFile != value)
                    {
                        base.SetProperty(ref this._inletFile, value);
                    }
                }
            }

            public string MSFile
            {
                get { return _MSFile; }
                set
                {
                    if (this._MSFile != value)
                    {
                        base.SetProperty(ref this._MSFile, value);
                    }
                }
            }

            public double InjectVolume
            {
                get { return _iInjectVolume; }
                set
                {
                    if (this._iInjectVolume != value)
                    {
                        base.SetProperty(ref this._iInjectVolume, value);
                    }
                }
            }

            public string ParameterFile
            {
                get { return _parameterFile; }
                set
                {
                    if (this._parameterFile != value)
                    {
                        base.SetProperty(ref this._parameterFile, value);
                    }
                }
            }

            public string Process
            {
                get { return _process; }
                set
                {
                    if (this._process != value)
                    {
                        base.SetProperty(ref this._process, value);
                    }
                }
            }

            internal static bool IsNullOrEmthy(Parameter parameterToAdd)
            {
                if (parameterToAdd == null)
                {
                    return true;
                }

                if (String.IsNullOrWhiteSpace(parameterToAdd.Analysis) && String.IsNullOrWhiteSpace(parameterToAdd.InletFile)
                    && String.IsNullOrWhiteSpace(parameterToAdd.MSFile) && String.IsNullOrWhiteSpace(parameterToAdd.ParameterFile)
                     && String.IsNullOrWhiteSpace(parameterToAdd.Process) && parameterToAdd.InjectVolume == 0D)
                {
                    return true;
                }

                return false;
            }
        }

        [Serializable]
        public class Standard : BaseModel
        {
            private string _analysis;
            private string _sampleId;
            private string _bottle;
            private string _inletPrerun;
            private string _inletFile;
            private string _MSFile;
            private string _MassA;
            private string _MassB;
            private string _notes;

            public string Analysis
            {
                get { return _analysis; }
                set
                {
                    if (this._analysis != value)
                    {
                        base.SetProperty(ref this._analysis, value);
                    }
                }
            }

            public string SampleId
            {
                get { return _sampleId; }
                set
                {
                    if (this._sampleId != value)
                    {
                        base.SetProperty(ref this._sampleId, value);
                    }
                }
            }

            public string Bottle
            {
                get { return _bottle; }
                set
                {
                    if (this._bottle != value)
                    {
                        base.SetProperty(ref this._bottle, value);
                    }
                }
            }

            public string InletPrerun
            {
                get { return _inletPrerun; }
                set
                {
                    if (this._inletPrerun != value)
                    {
                        base.SetProperty(ref this._inletPrerun, value);
                    }
                }
            }

            public string InletFile
            {
                get { return _inletFile; }
                set
                {
                    if (this._inletFile != value)
                    {
                        base.SetProperty(ref this._inletFile, value);
                    }
                }
            }

            public string MSFile
            {
                get { return _MSFile; }
                set
                {
                    if (this._MSFile != value)
                    {
                        base.SetProperty(ref this._MSFile, value);
                    }
                }
            }

            public string MassA
            {
                get { return _MassA; }
                set
                {
                    if (this._MassA != value)
                    {
                        base.SetProperty(ref this._MassA, value);
                    }
                }
            }

            public string MassB
            {
                get { return _MassB; }
                set
                {
                    if (this._MassB != value)
                    {
                        base.SetProperty(ref this._MassB, value);
                    }
                }
            }

            public string Notes
            {
                get { return _notes; }
                set
                {
                    if (this._notes != value)
                    {
                        base.SetProperty(ref this._notes, value);
                    }
                }
            }

            internal static bool IsNullOrEmthy(Standard standardToAdd)
            {
                if (standardToAdd == null)
                {
                    return true;
                }

                if (String.IsNullOrWhiteSpace(standardToAdd.Analysis) && String.IsNullOrWhiteSpace(standardToAdd.SampleId)
                    && String.IsNullOrWhiteSpace(standardToAdd.Bottle) && String.IsNullOrWhiteSpace(standardToAdd.InletPrerun)
                    && String.IsNullOrWhiteSpace(standardToAdd.InletFile) && String.IsNullOrWhiteSpace(standardToAdd.MSFile)
                    && String.IsNullOrWhiteSpace(standardToAdd.MassA) && String.IsNullOrWhiteSpace(standardToAdd.MassB)
                    && String.IsNullOrWhiteSpace(standardToAdd.Notes))
                {
                    return true;
                }

                return false;
            }
        }

        [Serializable]
        public class Volume : BaseModel
        {
            private string _analysis;
            private string _iInlet;
            private double _flowRate;
            private double _runTime;
            private double _mSFlowRate;

            public string Analysis
            {
                get { return _analysis; }
                set
                {
                    if (this._analysis != value)
                    {
                        base.SetProperty(ref this._analysis, value);
                    }
                }
            }

            public string Inlet
            {
                get { return _iInlet; }
                set
                {
                    if (this._iInlet != value)
                    {
                        base.SetProperty(ref this._iInlet, value);
                    }
                }
            }

            /// <summary>
            /// Flow Rate (ml/min)
            /// </summary>
            public double FlowRate
            {
                get { return _flowRate; }
                set
                {
                    if (this._flowRate != value)
                    {
                        base.SetProperty(ref this._flowRate, value);
                    }
                }
            }

            /// <summary>
            /// Run time (min)
            /// </summary>
            public double RunTime
            {
                get { return _runTime; }
                set
                {
                    if (this._runTime != value)
                    {
                        base.SetProperty(ref this._runTime, value);
                    }
                }
            }

            /// <summary>
            /// MS Flow Rate (ml/min)
            /// </summary>
            public double MSFlowRate
            {
                get { return _mSFlowRate; }
                set
                {
                    if (this._mSFlowRate != value)
                    {
                        base.SetProperty(ref this._mSFlowRate, value);
                    }
                }
            }

            internal static bool IsNullOrEmthy(Volume volumeToAdd)
            {
                if (volumeToAdd == null)
                {
                    return true;
                }

                if (String.IsNullOrWhiteSpace(volumeToAdd.Analysis) && String.IsNullOrWhiteSpace(volumeToAdd.Inlet)
                    && volumeToAdd.FlowRate == 0D && volumeToAdd.RunTime == 0D && volumeToAdd.MSFlowRate == 0D)
                {
                    return true;
                }

                return false;
            }
        }

        public ObservableCollection<Parameter> Parameters { get; } = new ObservableCollection<Parameter>();

        public ObservableCollection<Standard> Standards { get; } = new ObservableCollection<Standard>();

        public ObservableCollection<Volume> Volumes { get; } = new ObservableCollection<Volume>();

        public object Clone()
        {
            ConfigurationFileModel cloned = default(ConfigurationFileModel);

            using (var memoryStream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(memoryStream, this);
                memoryStream.Seek(0, SeekOrigin.Begin);
                cloned = (ConfigurationFileModel)formatter.Deserialize(memoryStream);
                memoryStream.Close();

            }

            return cloned;
        }
    }
}
