﻿using DocumentFormat.OpenXml.Spreadsheet;
using IntelliScanToMasslynx.WPF.Comparers;
using IntelliScanToMasslynx.WPF.Converters;
using IntelliScanToMasslynx.WPF.Excel.OpenXml;
using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class PhyschemResultFileGenerator : IDisposable
    {
        private const string HSA = "HSA";
        private const string LOGD = "LogD";
        private const string IAM = "IAM";
        
        private CancellationTokenSource _cancellationTokenSource;
        private string _documentPath;
        private ExcelManager _excelManager;
        private string _templatePath;
        private IList<IntelliScanFileModel> _intelliScanFileData;
        private IList<IntelliScanFileModel> _intelliScanFileDataOrdered;
        private ConfigurationFileModel _configuration;
        private IList<MassLynxSamplesFileWorklistSheetModel> _massLynxSamplesFileWorklistSheetModel;

        public PhyschemResultFileGenerator(string documentPath, string templatePath, IList<IntelliScanFileModel> intelliScanFileData, IList<IntelliScanFileModel> intelliScanFileDataOrdered,
            ConfigurationFileModel configuration, IList<MassLynxSamplesFileWorklistSheetModel> massLynxSamplesFileWorklistSheetModel, CancellationTokenSource cancellationTokenSource)
        {
            if (String.IsNullOrWhiteSpace(documentPath))
            {
                throw new ArgumentNullException(nameof(documentPath), "The physchem excel file to be created path cannot be null or empty.");
            }
            if (!Path.GetExtension(documentPath).ToLower().Equals(".xlsx"))
            {
                throw new ArgumentException($"The physchem file to be created is not an excel file. The path: '{documentPath}'", nameof(documentPath));
            }
            if (String.IsNullOrWhiteSpace(templatePath))
            {
                throw new ArgumentNullException(nameof(templatePath), "The existing physchem template excel file path cannot be null or empty.");
            }
            if (!Path.GetExtension(templatePath).ToLower().Equals(".xlsx"))
            {
                throw new ArgumentException($"The existing physchm template file is not an excel file. The path: '{templatePath}'", nameof(templatePath));
            }
            if (cancellationTokenSource == null)
            {
                throw new ArgumentNullException(nameof(cancellationTokenSource));
            }
            if (intelliScanFileData?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileData));
            }
            if (intelliScanFileDataOrdered?.Count == 0)
            {
                throw new ArgumentNullException(nameof(intelliScanFileDataOrdered));
            }
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            if (configuration.Parameters?.Count == 0 || configuration.Standards?.Count == 0 || configuration.Volumes?.Count == 0)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            if (massLynxSamplesFileWorklistSheetModel?.Count == 0)
            {
                throw new ArgumentNullException(nameof(massLynxSamplesFileWorklistSheetModel));
            }

            this._documentPath = documentPath;
            this._templatePath = templatePath;
            this._cancellationTokenSource = cancellationTokenSource;
            this._excelManager = new ExcelManager(this._documentPath, this._templatePath, true);
            this._intelliScanFileData = intelliScanFileData;
            this._intelliScanFileDataOrdered = intelliScanFileDataOrdered;
            this._configuration = configuration;
            this._massLynxSamplesFileWorklistSheetModel = massLynxSamplesFileWorklistSheetModel;

            this._excelManager.GetWorkbook().CalculationProperties.ForceFullCalculation = true;
            this._excelManager.GetWorkbook().CalculationProperties.FullCalculationOnLoad = true;

            this.Validate();
        }

        private void Validate()
        {
            IList<string> worksheetNames = this._excelManager.GetWorksheetNames();
            if (worksheetNames?.Count == 0)
            {
                throw new PLAErrorException("The given physchem file is invalid.", null);
            }
            if (worksheetNames.Count != 10)
            {
                throw new PLAErrorException("The given physchem has invalid sheets.", null);
            }
            if (!worksheetNames[0].Equals(Constants.SubmittedSamplesSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The first sheet is not the submitted samples sheet.", null);
            }
            if (!worksheetNames[1].Equals(Constants.HSASheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The second sheet is not the HSA sheet.", null);
            }
            if (!worksheetNames[2].Equals(Constants.HSACurveSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The third sheet is not the HSA curve sheet.", null);
            }
            if (!worksheetNames[3].Equals(Constants.LogDSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 4th sheet is not the log D sheet.", null);
            }
            if (!worksheetNames[4].Equals(Constants.LogDCurveSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 5th sheet is not the log D curve sheet.", null);
            }
            if (!worksheetNames[5].Equals(Constants.IAMSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 6th sheet is not the IAM sheet.", null);
            }
            if (!worksheetNames[6].Equals(Constants.IAMCurveSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 7th sheet is not the IAM curve sheet.", null);
            }
            if (!worksheetNames[7].Equals(Constants.VOfDistributionSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 8th sheet is not the V of Distribution sheet.", null);
            }
            if (!worksheetNames[8].Equals(Constants.PharonUploadSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 9th sheet is not the Pharon upload sheet.", null);
            }
            if (!worksheetNames[9].Equals(Constants.ResultsforchemistsSheetName))
            {
                throw new PLAErrorException("The given physchem file is invalid. The 10th sheet is not the Results chemists sheet.", null);
            }
        }

        public void Do()
        {
            #region Adding style formats

            this._excelManager.AddCellFormat(CellFormatHelper.GENERAL_PHYSCHEM_FILE, CellFormatHelper.GetCellFormatComponentsByName(CellFormatHelper.GENERAL_PHYSCHEM_FILE));

            #endregion Adding style formats

            this.GenerateSubmittedSamples();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            this.GenerateHSA();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            this.GenerateLogD();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }

            this.GenerateIAM();
            if (this._cancellationTokenSource.IsCancellationRequested)
            {
                return;
            }
        }
        
        private void GenerateSubmittedSamples()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the Submitted Samples sheet.");

            IList<MassLynxSamplesFileWorklistSheetModel> data = new List<MassLynxSamplesFileWorklistSheetModel>();
            string sheetName = Constants.SubmittedSamplesSheetName;
            Cell currentCell;

            int rowCounter = 4;//first row is the header
            foreach (IntelliScanFileModel item in this._intelliScanFileDataOrdered.Where(x => !x.IsDuplicatedForDuplicatedSheet))
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return;
                }

                int columnCounter = 1;
                string wellLocation = null;
                try
                {
                    wellLocation = WellPlateWellPositionTranslator.GetWellPosition(item.WellLocation);
                }
                catch (Exception exception)
                {
                    Utilities.Instance.Log(LogLevels.Warning, $"Could not been determined the well location. Sample id: '{item.Compound}'", exception);
                }

                string protocolName = item.IsDuplicatedForWorklistSheet ? Constants.PROTOCOL_NAME_FOR_ALL : item?.ProtocolName;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter), CellValue = new CellValue(item?.Compound), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);
                columnCounter += 4;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter), CellValue = new CellValue(wellLocation), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);
                columnCounter += 2;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item?.UserName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(protocolName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.ExactMass.HasValue ? item.ExactMass.ToString() : ""), DataType = CellValues.Number };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                rowCounter++;
            }
            Utilities.Instance.Log(LogLevels.Information, "Finished generating the Submitted Samples sheet.");
        }

        private void GenerateHSA()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the HSA sheet.");

            var samples = this._intelliScanFileData.Select(y => y.Compound).ToList();
            var dataToInsert = this._massLynxSamplesFileWorklistSheetModel.Where(x => x.Parameter != null && x.Parameter.InletFile.Equals(HSA));

            string sheetName = Constants.HSASheetName;
            Cell currentCell;

            int rowCounter = 2;//first row is the header
            foreach (MassLynxSamplesFileWorklistSheetModel item in dataToInsert)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return;
                }

                int columnCounter = 1;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.FileName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.IntelliScanFileModel.Compound), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                rowCounter++;
            }

            Utilities.Instance.Log(LogLevels.Information, "Finished generating HSA sheet.");
        }

        private void GenerateLogD()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the Log D sheet.");

            var samples = this._intelliScanFileData.Select(y => y.Compound).ToList();
            var dataToInsert = this._massLynxSamplesFileWorklistSheetModel.Where(x => x.Parameter != null && x.Parameter.InletFile.Equals(LOGD));

            string sheetName = Constants.LogDSheetName;
            Cell currentCell;

            int rowCounter = 2;//first row is the header
            foreach (MassLynxSamplesFileWorklistSheetModel item in dataToInsert)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return;
                }

                int columnCounter = 1;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.FileName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.IntelliScanFileModel.Compound), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                rowCounter++;
            }

            Utilities.Instance.Log(LogLevels.Information, "Finished generating Log D sheet.");
        }

        private void GenerateIAM()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started generating the IAM sheet.");

            var samples = this._intelliScanFileData.Select(y => y.Compound).ToList();
            var dataToInsert = this._massLynxSamplesFileWorklistSheetModel.Where(x => x.Parameter != null && x.Parameter.InletFile.Equals(IAM));

            string sheetName = Constants.IAMSheetName;
            Cell currentCell;

            int rowCounter = 2;//first row is the header
            foreach (MassLynxSamplesFileWorklistSheetModel item in dataToInsert)
            {
                if (this._cancellationTokenSource.IsCancellationRequested)
                {
                    return;
                }

                int columnCounter = 1;

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.FileName), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                currentCell = new Cell() { CellReference = Helper.GetCellName(rowCounter, columnCounter++), CellValue = new CellValue(item.IntelliScanFileModel.Compound), DataType = CellValues.String };
                this._excelManager.CreateSpreadsheetCellIfNotExist(sheetName, currentCell);
                this._excelManager.ApplyCellFormat(currentCell, CellFormatHelper.GENERAL_PHYSCHEM_FILE);

                rowCounter++;
            }

            Utilities.Instance.Log(LogLevels.Information, "Finished generating IAM sheet.");
        }

        public void Dispose()
        {
            this._excelManager.SaveWorksheet(Constants.SubmittedSamplesSheetName);
            this._excelManager.SaveWorksheet(Constants.HSASheetName);
            this._excelManager.SaveWorksheet(Constants.HSACurveSheetName);
            this._excelManager.SaveWorksheet(Constants.LogDSheetName);
            this._excelManager.SaveWorksheet(Constants.LogDCurveSheetName);
            this._excelManager.SaveWorksheet(Constants.IAMSheetName);
            this._excelManager.SaveWorksheet(Constants.IAMCurveSheetName);
            this._excelManager.SaveWorksheet(Constants.VOfDistributionSheetName);
            this._excelManager.SaveWorksheet(Constants.PharonUploadSheetName);
            this._excelManager.SaveWorksheet(Constants.ResultsforchemistsSheetName);
            this._excelManager.Dispose();
            this._excelManager = null;
        }
    }
}
