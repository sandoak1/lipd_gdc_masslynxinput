﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class MassLynxSamplesFileWorklistSheetModel
    {
        public string FileName { get; set; }

        public string InletFile
        {
            get
            {
                if (this.Standard != null)
                {
                    return this.Standard.InletFile;
                }
                if (this.Parameter != null)
                {
                    return this.Parameter.InletFile;
                }

                return null;
            }
        }

        public IntelliScanFileModel IntelliScanFileModel { get; set; }

        public ConfigurationFileModel.Parameter Parameter { get; set; }

        public ConfigurationFileModel.Standard Standard { get; set; }
    }
}
