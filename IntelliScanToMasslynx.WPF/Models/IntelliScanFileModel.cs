﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class IntelliScanFileModel
    {
        public string WellLocation { get; set; }

        public string Compound { get; set; }

        public string UserName { get; set; }

        public string ProtocolName { get; set; }

        public double? ExactMass { get; set; }

        public uint RowNumber { get; set; }

        public bool IsDuplicatedForDuplicatedSheet { get; set; }

        public bool IsDuplicatedForWorklistSheet { get; set; }

        public int RowNumberInTheResultFile { get; set; }

        /// <summary>
        /// The protocol name (task) will be renamed to "Trend Analysis All".
        /// </summary>
        public bool ShouldUpdateProtocolName { get; internal set; }

        public const string SharedStringPropertyNamePostFix = "SharedString";

        internal static bool IsNullOrEmthy(IntelliScanFileModel intelliScanFileModel)
        {
            if (intelliScanFileModel == null)
            {
                return true;
            }

            if (String.IsNullOrWhiteSpace(intelliScanFileModel.WellLocation) && String.IsNullOrWhiteSpace(intelliScanFileModel.Compound)
                && String.IsNullOrWhiteSpace(intelliScanFileModel.UserName) && String.IsNullOrWhiteSpace(intelliScanFileModel.ProtocolName)
                && !intelliScanFileModel.ExactMass.HasValue)
            {
                return true;
            }

            return false;
        }
    }
}
