﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class Column
    {
        public string Name { get; set; }

        public bool IsRemanedName { get; set; }
    }
}
