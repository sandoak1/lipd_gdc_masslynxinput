﻿using DocumentFormat.OpenXml.Spreadsheet;
using IntelliScanToMasslynx.WPF.Excel.OpenXml;
using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Models
{
    public class ConfigurationFileReader : IDisposable
    {
        public class ReadResult
        {
            public string WorksheetName { get; set; }

            public int IncorrectRowCount { get; set; }

            public int CorrectRowCount { get; set; }
        }

        private string _filePath;
        private ExcelManager _excelManager;
        private IList<HeaderColumn> _headerColumns = new List<HeaderColumn>();
        private CancellationTokenSource _cancellationTokenSource;

        public IList<ReadResult> ReadResults { get; } = new List<ReadResult>();

        public IList<HeaderColumn> HeaderColumns { get { return this._headerColumns; } }


        public ConfigurationFileReader(string filePath, CancellationTokenSource cancelationTokenSource)
        {
            if (String.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentNullException(nameof(filePath), "The input excel file path cannot be null or empty.");
            }
            if (!Path.GetExtension(filePath).Equals(".xlsx"))
            {
                throw new ArgumentException($"The input file is not an excel file. The path: '{filePath}'", nameof(filePath));
            }
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("The given input file does not exists.", filePath);
            }
            if (cancelationTokenSource == null)
            {
                throw new ArgumentNullException(nameof(cancelationTokenSource));
            }

            this._filePath = filePath;
            this._excelManager = new ExcelManager(filePath);
            this._cancellationTokenSource = cancelationTokenSource;

            IList<string> worksheetNames = this._excelManager.GetWorksheetNames();

            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Analysis", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.Analysis) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Inlet File", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.InletFile) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "MS File", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.MSFile) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Inject Volume", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.InjectVolume) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Parameter File", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.ParameterFile) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Process", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Parameter.Process) });

            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Analysis", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.Analysis) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Sample ID", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.SampleId) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Bottle", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.Bottle) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Inlet Prerun", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.InletPrerun) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Inlet File", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.InletFile) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "MS File", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.MSFile) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Mass A", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.MassA) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Mass B", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.MassB) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Notes", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_STANDARD_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Standard.Notes) });

            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Analysis", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_VOLUME_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Volume.Analysis) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Inlet", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_VOLUME_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Volume.Inlet) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Flow Rate (ml/min)", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_VOLUME_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Volume.FlowRate) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "Run Time (min)", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_VOLUME_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Volume.RunTime) });
            this._headerColumns.Add(new HeaderColumn(new List<Column>(new[] { new Column { Name = "MS Flow Rate (ml/min)", IsRemanedName = false } })) { IsRequired = true, SheetName = Constants.CONFIGURATION_VOLUME_SHEET_NAME, ModelClassPropertyName = nameof(ConfigurationFileModel.Volume.MSFlowRate) });
        }

        public ConfigurationFileModel ReadData()
        {
            Utilities.Instance.Log(LogLevels.Information, "Started parsing the Configuration file.");

            IList<string> sheetNames = this._excelManager.GetWorksheetNames();

            if (sheetNames?.Count == 0)
            {
                throw new PLAErrorException($"The Configuration file has no sheet.", null);
            }

            string[] sheetNamesToRead = new[] { Constants.CONFIGURATION_PARAMETERS_SHEET_NAME, Constants.CONFIGURATION_STANDARD_SHEET_NAME, Constants.CONFIGURATION_VOLUME_SHEET_NAME };
            var existingWorksheets = this._excelManager.GetWorksheetNames();

            var missingWorksheets = sheetNamesToRead.Where(x => !existingWorksheets.Contains(x)).ToList();

            if (missingWorksheets.Count > 0)
            {
                throw new PLAErrorException($"The following sheet{(missingWorksheets.Count == 1 ? "" : "s")} {(missingWorksheets.Count == 1 ? "is" : "are")} missing. {String.Join(", ", missingWorksheets)}", null);
            }

            int rowNumber = 0;
            int incorrentRowCount = 0;
            int correntRowCount = 0;
            ConfigurationFileModel configuration = new ConfigurationFileModel();

            foreach (string worksheetName in sheetNamesToRead)
            {
                rowNumber = 0;
                incorrentRowCount = 0;
                correntRowCount = 0;
                IEnumerable<Row> rows = null;
                IList<HeaderColumn> currentHeaders = null;
                List<ConfigurationFileModel.Parameter> parameters = new List<ConfigurationFileModel.Parameter>();
                List<ConfigurationFileModel.Standard> standards = new List<ConfigurationFileModel.Standard>();
                List<ConfigurationFileModel.Volume> volumnes = new List<ConfigurationFileModel.Volume>();

                try
                {
                    SheetData sheetData = this._excelManager.GetSheetData(worksheetName);

                    rows = this._excelManager.GetRows(worksheetName);
                }
                catch (Exception exception)
                {
                    Utilities.Instance.Log(LogLevels.Error, $"Error occured during reading the sheet: '{worksheetName}'", exception);
                }

                foreach (Row row in rows)
                {
                    if (this._cancellationTokenSource.IsCancellationRequested)
                    {
                        return null;
                    }

                    rowNumber++;

                    if (rowNumber == 1)
                    {
                        currentHeaders = ValidateHeaderRow(row, worksheetName).ToList();
                        continue;
                    }

                    IList<Cell> rowCells = this._excelManager.GetRowCells(row);

                    try
                    {
                        ConfigurationFileModel.Parameter parameterToAdd = new ConfigurationFileModel.Parameter();
                        ConfigurationFileModel.Standard standardToAdd = new ConfigurationFileModel.Standard();
                        ConfigurationFileModel.Volume volumeToAdd = new ConfigurationFileModel.Volume();

                        for (int i = 0; i < currentHeaders.Count; i++)
                        {
                            if (!currentHeaders[i].IsRequired && !currentHeaders[i].IsUsed)
                            {
                                continue;
                            }

                            string propertyName = currentHeaders[i].ModelClassPropertyName;
                            string cellName = $"{currentHeaders[i].ColumnNameInExcel.SingleOrDefault(x => x.SheetName.Equals(worksheetName)).ColumnNameTitle}{row.RowIndex.InnerText}";
                            Cell cell = rowCells.SingleOrDefault(x => x.CellReference.Value.Equals(cellName));
                            CellValuePatient cellValue = this._excelManager.GetCellValue(cell);
                            
                            if (worksheetName.Equals(Constants.CONFIGURATION_PARAMETERS_SHEET_NAME))
                            {
                                this.SetProperty(parameterToAdd, propertyName, cellValue, (int)row.RowIndex.Value);
                            }
                            if (worksheetName.Equals(Constants.CONFIGURATION_STANDARD_SHEET_NAME))
                            {
                                this.SetProperty(standardToAdd, propertyName, cellValue, (int)row.RowIndex.Value);
                            }
                            if (worksheetName.Equals(Constants.CONFIGURATION_VOLUME_SHEET_NAME))
                            {
                                this.SetProperty(volumeToAdd, propertyName, cellValue, (int)row.RowIndex.Value);
                            }
                        }

                        if (worksheetName.Equals(Constants.CONFIGURATION_PARAMETERS_SHEET_NAME) && ConfigurationFileModel.Parameter.IsNullOrEmthy(parameterToAdd))
                        {
                            continue;
                        }
                        if (worksheetName.Equals(Constants.CONFIGURATION_STANDARD_SHEET_NAME) && ConfigurationFileModel.Standard.IsNullOrEmthy(standardToAdd))
                        {
                            continue;
                        }
                        if (worksheetName.Equals(Constants.CONFIGURATION_VOLUME_SHEET_NAME) && ConfigurationFileModel.Volume.IsNullOrEmthy(volumeToAdd))
                        {
                            continue;
                        }

                        if (worksheetName.Equals(Constants.CONFIGURATION_PARAMETERS_SHEET_NAME))
                        {
                            configuration.Parameters.Add(parameterToAdd);
                            correntRowCount++;
                        }
                        if (worksheetName.Equals(Constants.CONFIGURATION_STANDARD_SHEET_NAME))
                        {
                            configuration.Standards.Add(standardToAdd);
                            correntRowCount++;
                        }
                        if (worksheetName.Equals(Constants.CONFIGURATION_VOLUME_SHEET_NAME))
                        {
                            configuration.Volumes.Add(volumeToAdd);
                            correntRowCount++;
                        }
                    }
                    catch (Exception exception)
                    {
                        incorrentRowCount++;
                        Utilities.Instance.Log(LogLevels.Error, $"Error occued parsing the {rowNumber}. line from the sheet '{worksheetName}' from the configuration file.", exception);
                    }
                }

                this.ReadResults.Add(new ReadResult { WorksheetName = worksheetName, IncorrectRowCount = incorrentRowCount, CorrectRowCount = correntRowCount });
                Utilities.Instance.Log(LogLevels.Information, $"{correntRowCount} lines have been parsed from the sheet '{worksheetName}'.");
                Utilities.Instance.Log(LogLevels.Warning, $"{incorrentRowCount} lines could not be parsed from the sheet '{worksheetName}'.");
            }

            Utilities.Instance.Log(LogLevels.Information, "Finished parsing the Configuration file.");
            return configuration;
        }
        
        private IEnumerable<HeaderColumn> ValidateHeaderRow(Row headerRow, string worksheetName)
        {
            if (String.IsNullOrWhiteSpace(worksheetName))
            {
                throw new ArgumentNullException(nameof(worksheetName));
            }
            if (headerRow.RowIndex.Value != 1)
            {
                throw new ArgumentException("The parameter is not the header row.", nameof(headerRow));
            }

            IList<Cell> headerCells = this._excelManager.GetRowCells(headerRow);
            IList<CellValuePatient> headerCellValues = this._excelManager.GetRowCellValues(headerRow);

            foreach (HeaderColumn headerColumn in this._headerColumns.Where(x => x.SheetName.Equals(worksheetName)))
            {
                IList<string> titlesToUpper = headerColumn.Titles.Select(x => x.Name.ToUpper()).ToList();
                Cell actualCell = headerCells.SingleOrDefault(x => titlesToUpper.Contains(this._excelManager.GetCellValue(x).Value.ToUpper()));
                if (actualCell == null && headerColumn.IsRequired)
                {
                    throw new PLAWarningException($"Invalid Configuration file. Please give a valid Configuration file. The column named '{headerColumn.Titles.First(x => !x.IsRemanedName).Name}' is missing from the worksheet named '{worksheetName}'.", null);
                }
                if (actualCell == null)
                {
                    continue;
                }

                string columnName = Helper.GetColumnName(actualCell.CellReference.Value);
                headerColumn.ColumnNameInExcel.Add(new ColumnName { ColumnNameTitle = columnName, SheetName = worksheetName });
            }

            IList<string> optionalHeaderTitles = this._headerColumns.Where(x => !x.IsRequired).Select(x => x.Titles.Select(y => y.Name.ToUpper())).SelectMany(x => x).ToList();
            IList<string> optionalHeaderTitlesFromTheFiles = headerCells.Select(x => this._excelManager.GetCellValue(x)).Select(x => x.Value.ToUpper()).Where(x => optionalHeaderTitles.Contains(x)).ToList();
            if (optionalHeaderTitlesFromTheFiles.Count > 0)
            {
                var usedOptionalHeaderTitleInTheFiles = optionalHeaderTitles.Where(x => optionalHeaderTitlesFromTheFiles.Select(y => y.ToUpper()).Contains(x.ToUpper())).ToList();

                var usedColumns = this._headerColumns.Where(x => x.IsRequired
                    || (!x.IsRequired && x.Titles.Select(y => y.Name.ToUpper()).Any(z => usedOptionalHeaderTitleInTheFiles.Contains(z)))
                ).ToList();

                foreach (HeaderColumn item in usedColumns.Where(x => !x.IsRequired))
                {
                    item.IsUsed = true;
                }
                return usedColumns;
            }

            return this._headerColumns.Where(x => x.IsRequired && x.SheetName.Equals(worksheetName)).ToList();
        }

        private void SetProperty(ConfigurationFileModel.BaseModel intelliScanFileModel, string propertyName, CellValuePatient value, int rowNumber)
        {
            if (value == null)
            {
                value = new CellValuePatient();
            }
            int parsedNumber = -1;
            double parseNumberDouble = -1D;
            DateTime parsedDatetime = DateTime.MinValue;

            PropertyInfo propertyToSet = intelliScanFileModel.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);

            if (propertyToSet.PropertyType.Equals(typeof(string)))
            {
                if (value.CellValues == CellValues.SharedString)
                {
                    propertyToSet.SetValue(intelliScanFileModel, value.Value, null);
                    PropertyInfo propertyToSetSharedString = intelliScanFileModel.GetType().GetProperty($"{propertyName}{IntelliScanFileModel.SharedStringPropertyNamePostFix}", BindingFlags.Public | BindingFlags.Instance);
                    propertyToSetSharedString?.SetValue(intelliScanFileModel, value.OuterXml, null);
                }
                else
                {
                    propertyToSet.SetValue(intelliScanFileModel, value.Value, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(int?)))
            {
                if (Int32.TryParse(value.Value, out parsedNumber))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedNumber, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(int)))
            {
                if (Int32.TryParse(value.Value, out parsedNumber))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedNumber, null);
                }
                else
                {
                    string message = $"The Configuration file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message);
                    throw new Exception(message);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(double?)))
            {
                if (Double.TryParse(value.Value, out parseNumberDouble))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parseNumberDouble, null);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(double)))
            {
                if (Double.TryParse(value.Value, out parseNumberDouble))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parseNumberDouble, null);
                }
                else
                {
                    string message = $"The Configuration file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message);
                    throw new Exception(message);
                }
            }
            else if (propertyToSet.PropertyType.Equals(typeof(DateTime?)))
            {
                if (DateTime.TryParse(value.Value, out parsedDatetime))
                {
                    propertyToSet.SetValue(intelliScanFileModel, parsedDatetime, null);
                }
                else
                {
                    if (Double.TryParse(value.Value, out parseNumberDouble))
                    {
                        propertyToSet.SetValue(intelliScanFileModel, DateTime.FromOADate(parseNumberDouble), null);
                    }
                }
                //propertyToSet.SetValue(studyItem, null, null);
            }
            else if (propertyToSet.PropertyType.Equals(typeof(bool?)))
            {
                bool? valueAsBool = String.IsNullOrWhiteSpace(value.Value) ? default(bool?) : (value.Value.ToUpper().Equals("Y") || value.Value.ToUpper().Equals("YES") || value.Value.ToUpper().Equals("TRUE")) ? true : false;
                propertyToSet.SetValue(intelliScanFileModel, valueAsBool, null);
            }
            else if (propertyToSet.PropertyType.Equals(typeof(bool)))
            {
                try
                {
                    bool valueAsBool = value.Value.ToUpper().Equals("Y") ? true : false;
                    propertyToSet.SetValue(intelliScanFileModel, valueAsBool, null);
                }
                catch (Exception exception)
                {
                    string message = $"The Configuration file has invalid property ('{propertyToSet.Name}') at line: {rowNumber}.";
                    Utilities.Instance.Log(LogLevels.Error, message, exception);
                    propertyToSet.SetValue(intelliScanFileModel, false, null);
                }
            }
        }

        public void Dispose()
        {
            this._excelManager.Dispose();
            this._excelManager = null;
        }
    }
}
