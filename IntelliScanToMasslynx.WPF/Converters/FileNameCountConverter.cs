﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace IntelliScanToMasslynx.WPF.Converters
{
    public class FileNameCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            uint valueAsUint = (uint)value;
            if (valueAsUint == 0U)
            {
                return String.Empty;
            }

            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string valueAsString = value?.ToString();
            if (String.IsNullOrWhiteSpace(valueAsString))
            {
                return 0U;
            }

            uint parsedValue = 0;
            if (UInt32.TryParse(valueAsString, out parsedValue))
            {
                return parsedValue;
            }

            return 0U;
        }
    }
}
