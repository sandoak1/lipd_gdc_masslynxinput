﻿using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Converters
{
    public static class WellPlateWellPositionTranslator
    {
        public static string GetWellPosition(string wellLocation)
        {
            if (String.IsNullOrWhiteSpace(wellLocation))
            {
                return null;
            }

            Regex regex = new Regex(@"([a-zA-Z]{1})(\d{1,4})");

            if (!regex.IsMatch(wellLocation))
            {
                return null;
            }

            var match = regex.Match(wellLocation);

            string letter = null;
            string numbeAsString = null;
            foreach (Group matchGroup in match.Groups)
            {
                if (matchGroup.Value.Equals(wellLocation))
                {
                    continue;
                }

                if (matchGroup.Value.Length == 1)
                {
                    letter = matchGroup.Value;
                }
                if (matchGroup.Value.Length > 1)
                {
                    numbeAsString = matchGroup.Value;
                }
            }

            int parsedNumber = -1;
            if (Int32.TryParse(numbeAsString, out parsedNumber))
            {
                string translatedWellPosition = $"{DataModel.Instance.SelectedPlateNumber?.Value}:{letter},{parsedNumber}";
                return translatedWellPosition;
            }
            else
            {
                Utilities.Instance.Log(LogLevels.Warning, $"The well location '{wellLocation}' cannot be translated.");
                return null;
            }
        }
    }
}
