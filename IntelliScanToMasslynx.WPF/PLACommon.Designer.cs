﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntelliScanToMasslynx.WPF {
    
    
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "14.0.0.0")]
    internal sealed partial class PLACommon : global::System.Configuration.ApplicationSettingsBase {
        
        private static PLACommon defaultInstance = ((PLACommon)(global::System.Configuration.ApplicationSettingsBase.Synchronized(new PLACommon())));
        
        public static PLACommon Default {
            get {
                return defaultInstance;
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("%USERPROFILE%\\LipApps\\GDC_MassLynxInput\\Logs\\")]
        public string LogPath {
            get {
                return ((string)(this["LogPath"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://pla-dev.nibr.novartis.net/PLAErrorTracking/insert")]
        public string ErrorLoggerURL_TEST {
            get {
                return ((string)(this["ErrorLoggerURL_TEST"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://pla.nibr.novartis.net/PLAErrorTracking/insert")]
        public string ErrorLoggerURL {
            get {
                return ((string)(this["ErrorLoggerURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1")]
        public double UserVersion {
            get {
                return ((double)(this["UserVersion"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("IntelliScan to Masslynx Files Converter")]
        public string Application {
            get {
                return ((string)(this["Application"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("10")]
        public int LogFileCount {
            get {
                return ((int)(this["LogFileCount"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("1048576")]
        public long LogFileSize {
            get {
                return ((long)(this["LogFileSize"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Debug")]
        public string LogLevel {
            get {
                return ((string)(this["LogLevel"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level}] {MASTERNAME}: {Message} {Exception}" +
            "{NewLine}")]
        public string MessageTemplate {
            get {
                return ((string)(this["MessageTemplate"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://pla.nibr.novartis.net/PLATrackingService/PLATrackingTool.svc")]
        public string PLATrackingServiceURL {
            get {
                return ((string)(this["PLATrackingServiceURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("The windows application should help the user to create the Masslynx input and res" +
            "ult analysis files from IntelliScan output file. The outcome of the tool should " +
            "be a folder with two Excel files with different sheets used for the further anal" +
            "ysis.")]
        public string Description {
            get {
                return ((string)(this["Description"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [ERROR] {Message}{NewLine}{Exception}{NewLine" +
            "}MasterName:{MASTERNAME}{NewLine}User:{USER}{NewLine}Machine:{MACHINE}")]
        public string MessageTemplateRemote {
            get {
                return ((string)(this["MessageTemplateRemote"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://web.global.nibr.novartis.net/services/ps/v1/people")]
        public string PeopleServiceMultipleURLBase {
            get {
                return ((string)(this["PeopleServiceMultipleURLBase"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://web.global.nibr.novartis.net/services/ps/v1/people/search/")]
        public string PeopleServiceSearchURL {
            get {
                return ((string)(this["PeopleServiceSearchURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://web.global.nibr.novartis.net/services/ps/v1/person/")]
        public string PeopleServiceSinglePersonURL {
            get {
                return ((string)(this["PeopleServiceSinglePersonURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("Fatal")]
        public string RemoteLogLevel {
            get {
                return ((string)(this["RemoteLogLevel"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("GDC_MassLynxInput")]
        public string MasterName {
            get {
                return ((string)(this["MasterName"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("http://pla-usca.nibr.novartis.net/GenericPlaEmailServices/GenericEmailService.svc" +
            "")]
        public string GenericEmailServiceURL {
            get {
                return ((string)(this["GenericEmailServiceURL"]));
            }
        }
        
        [global::System.Configuration.ApplicationScopedSettingAttribute()]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.Configuration.DefaultSettingValueAttribute("True")]
        public bool LogToTrace {
            get {
                return ((bool)(this["LogToTrace"]));
            }
        }
    }
}
