﻿using IntelliScanToMasslynx.WPF.Models;
using Novartis.Ph.Nibr.Pla.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntelliScanToMasslynx.WPF
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginResult LoginResult { get; set; } = LoginResult.None;

        public string UserName { get; set; }

        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.txtUserName.Text))
            {
                MessageBox.Show("Please enter your username.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (String.IsNullOrWhiteSpace(this.pwbPassword.Password))
            {
                MessageBox.Show("Please enter your password.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            try
            {
                bool isSuccessfullLoggedIn = Controller.Instance.ValidateUserCredentials(txtUserName.Text, pwbPassword.Password, Properties.Settings.Default.DomainsForUsers.Cast<string>());
                if (isSuccessfullLoggedIn)
                {
                    this.UserName = txtUserName.Text;
                    Utilities.Instance.Log(LogLevels.Information, $"The user '{txtUserName.Text}' logged in successfully.");
                    this.LoginResult = LoginResult.Success;
                    this.txtUserName.Text = "";
                    this.pwbPassword.Password = "";
                    this.Close();
                }
                else
                {
                    this.pwbPassword.Password = "";
                    this.pwbPassword.Focus();
                    MessageBox.Show("Invalid username or password.", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception exception)
            {
                this.pwbPassword.Password = "";
                Utilities.Instance.Log(LogLevels.Information, $"Error occurred while logging in the user. username: '{txtUserName.Text}'", exception);
                MessageBox.Show("Error occurred while logging in.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.LoginResult = LoginResult.Cancel;
            this.txtUserName.Text = "";
            this.pwbPassword.Password = "";
            this.Close();
        }

        private void pwbPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.LoginButton_Click(this, new RoutedEventArgs());
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtUserName.Focus();
        }
    }
}
