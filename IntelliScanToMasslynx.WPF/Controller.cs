﻿using IntelliScanToMasslynx.WPF.Comparers;
using IntelliScanToMasslynx.WPF.Excel.Interop;
using IntelliScanToMasslynx.WPF.Models;
using IntelliScanToMasslynx.WPF.Properties;
using Novartis.Ph.Nibr.Pla.Common;
using Novartis.Ph.Nibr.Pla.Common.ExceptionHandling;
using Novartis.Ph.Nibr.Pla.WPFUtilities;
using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace IntelliScanToMasslynx.WPF
{
    public class Controller
    {
        public CancellationTokenSource CancellationTokenSource { get; private set; } = new CancellationTokenSource();

        private static Controller instance = new Controller();

        public static Controller Instance
        {
            get
            {
                return instance;
            }
        }

        private Window mainApplicationWindow = null;

        public Window MainApplicationWindow
        {
            get { return mainApplicationWindow; }
            set
            {
                mainApplicationWindow = value;
                Novartis.Ph.Nibr.Pla.WPFUtilities.WPFDataModel.MainApplicationWindow = mainApplicationWindow;
            }
        }

        private Controller()
        {
            Utilities.Instance.Log(LogLevels.Information, "Application started");
            Utilities.Instance.Log(LogLevels.Information, $"{Utilities.Instance.AssemblyTitle} v{Utilities.Instance.AssemblyVersion} started by '{Environment.UserName}' on machine '{Environment.MachineName}'");
            ExceptionHandler.RegisterHandler(new WPFExceptionHandler(null));

            DataModel.Instance.PlateNumbers.Add(new PlateNumberItem { Name = "1", Value = "1" });
            DataModel.Instance.PlateNumbers.Add(new PlateNumberItem { Name = "2", Value = "2" });
        }

        private void DeleteCreatedOutputFolder()
        {
            try
            {
                Directory.Delete(DataModel.Instance.OutputFolderPath, true);
            }
            catch (Exception exception)
            {
                string message = $"Error occurred during deleting the created output directory. The path: '{DataModel.Instance.OutputFolderPath}'";
                Utilities.Instance.Log(LogLevels.Error, message, exception);
                this.MainApplicationWindow.Dispatcher.BeginInvoke(
                            new Action(delegate
                            {
                                UIUtilities.AppendMessageToGUI(message, LogLevels.Error, true, true, new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red));
                            }), System.Windows.Threading.DispatcherPriority.Normal);
            }
        }

        public void Execute()
        {
            Utilities.Instance.Log(LogLevels.Information, "User entered the following details for the process: {break}{@data}", Environment.NewLine, DataModel.Instance);
            UIUtilities.ClearGUIMessages();

            IList<MassLynxSamplesFileWorklistSheetModel> massLynxSamplesFileWorklistSheetModels = null;
            DataModel.Instance.IsDone = false;
            DataModel.Instance.IsNotRunning = false;
            DataModel.Instance.IsStartButtonEnabled = false;
            string templatePathMassLynxSamples = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files", "Templates", "MassLynxSamples.xlsx");
            string outputFolderNameToCreate = this.GenerateOutputFolderName();
            DataModel.Instance.CreatedOutputFolderName = outputFolderNameToCreate;
            string directoryOutputPathToCreate = Path.Combine(DataModel.Instance.UserSelectedOutputFolderPath, outputFolderNameToCreate);
            DirectoryInfo diOutputDirectoryToCreate = null;
            try
            {
                diOutputDirectoryToCreate = Directory.CreateDirectory(directoryOutputPathToCreate);
            }
            catch (Exception exception)
            {
                string message = $"Creating the result directory ('{directoryOutputPathToCreate}') was not successfull.";
                UIUtilities.AppendMessageToGUI(message, LogLevels.Error, true, true, new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red));
                Utilities.Instance.Log(LogLevels.Error, message, exception);
                return;
            }
            if (diOutputDirectoryToCreate.Exists)
            {
                DataModel.Instance.OutputFolderPath = diOutputDirectoryToCreate.FullName;
                Utilities.Instance.Log(LogLevels.Information, $"The output directory successfully created. Path: '{diOutputDirectoryToCreate.FullName}'");
            }
            else
            {
                throw new PLAErrorException($"The outfout directory was not created successfully. Path: '{diOutputDirectoryToCreate.FullName}'", null);
            }

            string massLynxSamplesFilePath = Path.Combine(diOutputDirectoryToCreate.FullName, "MassLynxSampleInput.xlsx");
            string physchemFilePath = Path.Combine(diOutputDirectoryToCreate.FullName, "PhysChem.xlsx");

            ProgressHandler progressHandler = ProgressHandler.GetInstance(0D, 100D, 6);
            progressHandler.IncreaseWithOneStep();

            Task.Factory.StartNew(() =>
            {
                try
                {
                    UIUtilities.AppendMessageToGUI("Starting the process...");

                    UIUtilities.AppendMessageToGUI("Started reading the IntelliScan file.");

                    IntelliScanFileXmlReader intelliScanFileXmlReader = new IntelliScanFileXmlReader(DataModel.Instance.IntelliScanFilePath);
                    IList<IntelliScanFileModel> intelliScanFileData = intelliScanFileXmlReader.Read();
                    IList<IntelliScanFileModel> intelliScanFileDataOrdered = new List<IntelliScanFileModel>();
                    
                    //IntelliScanFileReader intelliScanFileReader = new IntelliScanFileReader(DataModel.Instance.IntelliScanFilePath, this.CancellationTokenSource);
                    //IList<IntelliScanFileModel> intelliScanFileData = intelliScanFileReader.ReadData();
                    progressHandler.IncreaseWithOneStep();
                    if (CancellationTokenSource.IsCancellationRequested)
                    {
                        DataModel.Instance.IsNotRunning = true;
                        UIUtilities.AppendMessageToGUI("Process  cancelled.", LogLevels.Information, true, true);
                        this.DeleteCreatedOutputFolder();
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = new CancellationTokenSource();
                        return;
                    }

                    UIUtilities.AppendMessageToGUI("Started reading the Configuration file.");
                    if (!DataModel.Instance.IsConfigurationLoaded)
                    {
                        ConfigurationFileReader configurationFileReader = new ConfigurationFileReader(DataModel.Instance.ConfigurationFilePath, this.CancellationTokenSource);
                        ConfigurationFileModel configuration = configurationFileReader.ReadData();
                        DataModel.Instance.Configuration = configuration;
                        DataModel.Instance.ConfigurationEdited = DataModel.Instance.Configuration.Clone() as ConfigurationFileModel;
                        DataModel.Instance.IsConfigurationLoaded = true;
                        progressHandler.IncreaseWithOneStep();
                        if (CancellationTokenSource.IsCancellationRequested)
                        {
                            DataModel.Instance.IsNotRunning = true;
                            UIUtilities.AppendMessageToGUI("Process cancelled.", LogLevels.Information, true, true);
                            this.DeleteCreatedOutputFolder();
                            this.CancellationTokenSource.Dispose();
                            this.CancellationTokenSource = new CancellationTokenSource();
                            return;
                        }
                    }

                    //The user can edit the configuration. After the he can run the tool once with the modified configuration.
                    ConfigurationFileModel configurationToUse = DataModel.Instance.Configuration;
                    if (DataModel.Instance.IsConfigurationEditedByTheUser && DataModel.Instance.RunCountAfterUserEditedTheConfiguration == 0U)
                    {
                        configurationToUse = DataModel.Instance.ConfigurationEdited;
                    }

                    #region Ordering intelliscanFileData

                    intelliScanFileDataOrdered = intelliScanFileData.OrderBy(x => x, new DataComparerByFirstBeforeAfterAndTaskNameForIntelliScanFileModel()).ToList();

                    #endregion Ordering intelliscanFileData

                    #region IntelliScan Masslynx file generation

                    UIUtilities.AppendMessageToGUI("Started generating the MassLynxSamplesInput excel file.");

                    using (MassLynxSamplesResultFileGenerator massLynxSamplesResultFileGenerator = new MassLynxSamplesResultFileGenerator(massLynxSamplesFilePath, templatePathMassLynxSamples,
                        intelliScanFileData, intelliScanFileDataOrdered, configurationToUse, DataModel.Instance.UserName, DataModel.Instance.MassLynxSamplesResultFile_FirstSampleInTheWorkList, CancellationTokenSource))
                    {
                        massLynxSamplesResultFileGenerator.Do();
                        massLynxSamplesFileWorklistSheetModels = massLynxSamplesResultFileGenerator.MassLynxSamplesFileWorklistSheetModels;
                    }
                    progressHandler.IncreaseWithOneStep();

                    if (CancellationTokenSource.IsCancellationRequested)
                    {
                        DataModel.Instance.IsNotRunning = true;
                        UIUtilities.AppendMessageToGUI("Process cancelled.", LogLevels.Information, true, true);
                        this.DeleteCreatedOutputFolder();
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = new CancellationTokenSource();
                        return;
                    }

                    #endregion IntelliScan Masslynx file generation

                    #region PhysChem file generation

                    UIUtilities.AppendMessageToGUI("Started generating the PhysChem excel file.");
                    using (PhyschemResultFileGenerator physchemResultFileGenerator = new PhyschemResultFileGenerator(physchemFilePath, DataModel.Instance.PhyChemFilePath,
                        intelliScanFileData, intelliScanFileDataOrdered, configurationToUse, massLynxSamplesFileWorklistSheetModels, CancellationTokenSource))
                    {
                        physchemResultFileGenerator.Do();
                    }
                    progressHandler.IncreaseWithOneStep();

                    if (CancellationTokenSource.IsCancellationRequested)
                    {
                        DataModel.Instance.IsNotRunning = true;
                        UIUtilities.AppendMessageToGUI("Process cancelled.", LogLevels.Information, true, true);
                        this.DeleteCreatedOutputFolder();
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = new CancellationTokenSource();
                        return;
                    }

                    using (PhyschemResultFileGeneratorInterop physChemInteropGenerator = new PhyschemResultFileGeneratorInterop(physchemFilePath, intelliScanFileData, intelliScanFileDataOrdered))
                    {
                        physChemInteropGenerator.Genereaete();
                    }
                    progressHandler.IncreaseWithOneStep();

                    if (CancellationTokenSource.IsCancellationRequested)
                    {
                        DataModel.Instance.IsNotRunning = true;
                        UIUtilities.AppendMessageToGUI("Process cancelled.", LogLevels.Information, true, true);
                        this.DeleteCreatedOutputFolder();
                        this.CancellationTokenSource.Dispose();
                        this.CancellationTokenSource = new CancellationTokenSource();
                        return;
                    }

                    #endregion PhysChem file generation

                    //test
                    Utilities.Instance.Log(LogLevels.Information, "Started calling the PLA tracking service.");
                    Utilities.Instance.CallPLATrackingService(DataModel.Instance.UserName);

                    progressHandler.IncreaseWithOneStep();

                    UIUtilities.AppendMessageToGUI("The process ended successfully.", LogLevels.Information, true, true);

                    DataModel.Instance.IsNotRunning = true;
                    DataModel.Instance.IsDone = true;
                    if (DataModel.Instance.IsConfigurationEditedByTheUser)
                    {
                        DataModel.Instance.RunCountAfterUserEditedTheConfiguration++;
                    }
                }
                catch (Exception ex)
                {
                    this.MainApplicationWindow.Dispatcher.BeginInvoke(
                            new Action(delegate
                            {
                                UIUtilities.AppendMessageToGUI(ex.Message, LogLevels.Error, true, true, new System.Windows.Media.SolidColorBrush(System.Windows.Media.Colors.Red));
                            }), System.Windows.Threading.DispatcherPriority.Normal);
                    ExceptionHandler.HandleException(ex);
                }
                finally
                {
                    DataModel.Instance.IsNotRunning = true;
                    DataModel.Instance.IsStartButtonEnabled = true;
                }
            });
        }

        private string GenerateOutputFolderName()
        {
            if (String.IsNullOrWhiteSpace(DataModel.Instance.UserSelectedOutputFolderPath))
            {
                throw new ArgumentNullException(nameof(DataModel.Instance.UserSelectedOutputFolderPath));
            }
            if (!Directory.Exists(DataModel.Instance.UserSelectedOutputFolderPath))
            {
                throw new DirectoryNotFoundException($"The given directory is not exist. Path: '{DataModel.Instance.UserSelectedOutputFolderPath}'");
            }

            string dateFormat = "yyyy_MM_dd";
            string directoryNameToCreate = DateTime.Now.ToString(dateFormat);
            string directoryPathToCreate = Path.Combine(DataModel.Instance.UserSelectedOutputFolderPath, directoryNameToCreate);
            int count = 0;
            do
            {
                count++;
                directoryPathToCreate = Path.Combine(DataModel.Instance.UserSelectedOutputFolderPath, directoryNameToCreate);
                if (Directory.Exists(directoryPathToCreate))
                {
                    directoryNameToCreate = $"{DateTime.Now.ToString(dateFormat)}-{count}";
                }
                else
                {
                    break;
                }
                if (count > 1000)
                {
                    throw new PLAErrorException($"The directory to be created ('{directoryPathToCreate}') is exists. The count after the date is more than 1000.", null);
                }
            } while (true);

            return directoryNameToCreate;
        }

        internal void OpenResult()
        {
            try
            {
                System.Diagnostics.Process.Start(DataModel.Instance.UserSelectedOutputFolderPath);
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
        }

        internal void BrowseIntelliScanFile()
        {
            try
            {
                string lastPath = Settings.Default.LastPathIntelliScanFile;
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathConfigurationFile;
                }
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathPhysChemFile;
                }
                if (!String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(lastPath);
                    }
                    catch (Exception exception)
                    {
                        
                    }
                }
                if (String.IsNullOrWhiteSpace(lastPath) && !String.IsNullOrWhiteSpace(Settings.Default.LastPathOutputFolder))
                {
                    lastPath = Settings.Default.LastPathOutputFolder;
                }
                //string result = UIUtilities.BrowseInputFile(lastPath, false, true, "Excel files (*.xlsx)|*.xlsx;").FirstOrDefault();
                string result = UIUtilities.BrowseInputFile(lastPath, false, true, "Xml files (*.xml)|*.xml;").FirstOrDefault();
                if (result != null)
                {
                    DataModel.Instance.IntelliScanFilePath = result;
                    Settings.Default.LastPathIntelliScanFile = result;
                    Settings.Default.Save();
                    Utilities.Instance.Log(LogLevels.Information, String.Format("IntelliScan file path: {0}", DataModel.Instance.IntelliScanFilePath));
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
        }

        internal void BrowseConfigurationFile()
        {
            try
            {
                string lastPath = Settings.Default.LastPathConfigurationFile;
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathIntelliScanFile;
                }
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathPhysChemFile;
                }
                if (!String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(lastPath);
                    }
                    catch (Exception exception)
                    {
                        
                    }
                }
                if (String.IsNullOrWhiteSpace(lastPath) && !String.IsNullOrWhiteSpace(Settings.Default.LastPathOutputFolder))
                {
                    lastPath = Settings.Default.LastPathOutputFolder;
                }
                string result = UIUtilities.BrowseInputFile(lastPath, false, true, "Excel files (*.xlsx)|*.xlsx;").FirstOrDefault();
                if (result != null)
                {
                    DataModel.Instance.ConfigurationFilePath = result;
                    Settings.Default.LastPathConfigurationFile = result;
                    Settings.Default.Save();
                    Utilities.Instance.Log(LogLevels.Information, String.Format("Configuration file path: {0}", DataModel.Instance.ConfigurationFilePath));
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
        }

        internal void BrowsePhyChemFile()
        {
            try
            {
                string lastPath = Settings.Default.LastPathPhysChemFile;
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathConfigurationFile;
                }
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    lastPath = Settings.Default.LastPathIntelliScanFile;
                }
                if (!String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(lastPath);
                    }
                    catch (Exception exception)
                    {
                        
                    }
                }
                if (String.IsNullOrWhiteSpace(lastPath) && !String.IsNullOrWhiteSpace(Settings.Default.LastPathOutputFolder))
                {
                    lastPath = Settings.Default.LastPathOutputFolder;
                }
                string result = UIUtilities.BrowseInputFile(lastPath, false, true, "Excel files (*.xlsx)|*.xlsx;").FirstOrDefault();
                if (result != null)
                {
                    DataModel.Instance.PhyChemFilePath = result;
                    Settings.Default.LastPathPhysChemFile = result;
                    Settings.Default.Save();
                    Utilities.Instance.Log(LogLevels.Information, String.Format("PhyChem file path: {0}", DataModel.Instance.PhyChemFilePath));
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
        }

        internal void BrowseOutputFolder()
        {
            try
            {
                string lastPath = Settings.Default.LastPathOutputFolder;
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(Settings.Default.LastPathIntelliScanFile);
                    }
                    catch (Exception exception)
                    {
                        
                    }
                }
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(Settings.Default.LastPathConfigurationFile);
                    }
                    catch (Exception exception)
                    {

                    }
                }
                if (String.IsNullOrWhiteSpace(lastPath))
                {
                    try
                    {
                        lastPath = Path.GetDirectoryName(Settings.Default.LastPathPhysChemFile);
                    }
                    catch (Exception exception)
                    {

                    }
                }
                string result = UIUtilities.BrowseFolder(lastPath);
                if (result != null)
                {
                    DataModel.Instance.UserSelectedOutputFolderPath = result;
                    Settings.Default.LastPathOutputFolder = result;
                    Settings.Default.Save();
                    Utilities.Instance.Log(LogLevels.Information, String.Format("Output folder path: {0}", DataModel.Instance.UserSelectedOutputFolderPath));
                }
            }
            catch (Exception ex)
            {
                ExceptionHandler.HandleException(ex);
            }
        }

        internal bool ValidateUserCredentials(string user521, string password, IEnumerable<string> domains)
        {
            foreach (string domain in domains)
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain))
                {
                    if (pc.ValidateCredentials(user521, password, ContextOptions.Negotiate))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
