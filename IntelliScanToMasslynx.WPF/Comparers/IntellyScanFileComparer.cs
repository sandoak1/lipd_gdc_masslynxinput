﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class IntellyScanFileComparer : IEqualityComparer<IntelliScanFileModel>
    {
        public bool Equals(IntelliScanFileModel x, IntelliScanFileModel y)
        {
            if (x == null && y == null)
            {
                return true;
            }
            if (x == null && y != null)
            {
                return false;
            }
            if (x != null && y == null)
            {
                return false;
            }

            bool isEquals = x.Compound.Equals(y.Compound);
            return isEquals;
        }

        public int GetHashCode(IntelliScanFileModel obj)
        {
            if (obj == null)
            {
                return 0;
            }

            return obj.Compound.GetHashCode();
        }
    }
}
