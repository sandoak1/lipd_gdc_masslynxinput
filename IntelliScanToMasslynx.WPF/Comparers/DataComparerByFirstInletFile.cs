﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class DataComparerByFirstInletFile : IComparer<MassLynxSamplesFileWorklistSheetModel>
    {
        private readonly List<string> INLET_ORDER = new List<string> { "HSA", "LogD", "IAM" };

        public int Compare(MassLynxSamplesFileWorklistSheetModel x, MassLynxSamplesFileWorklistSheetModel y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }

            if (String.IsNullOrWhiteSpace(x.InletFile) && String.IsNullOrWhiteSpace(y.InletFile))
            {
                return 0;
            }
            if (String.IsNullOrWhiteSpace(x.InletFile) && !String.IsNullOrWhiteSpace(y.InletFile))
            {
                return 1;
            }
            if (!String.IsNullOrWhiteSpace(x.InletFile) && String.IsNullOrWhiteSpace(y.InletFile))
            {
                return -1;
            }

            int indexX = INLET_ORDER.IndexOf(INLET_ORDER.FirstOrDefault(xx => x.InletFile.ToLower().StartsWith(xx.ToLower())));
            int indexY = INLET_ORDER.IndexOf(INLET_ORDER.FirstOrDefault(xx => y.InletFile.ToLower().StartsWith(xx.ToLower())));
            if (indexX == -1 && indexY == -1)
            {
                return 0;
            }
            if (indexX == -1 && indexY > -1)
            {
                return 1;
            }
            if (indexX > -1 && indexY == -1)
            {
                return -1;
            }

            if (indexX == indexY)
            {
                return 0;
            }
            if (indexX < indexY)
            {
                return -1;
            }
            if (indexX > indexY)
            {
                return 1;
            }

            return 0;
        }
    }
}
