﻿using IntelliScanToMasslynx.WPF.Converters;
using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class DuplicatedComparer : IComparer<IntelliScanFileModel>
    {
        public int Compare(IntelliScanFileModel x, IntelliScanFileModel y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }


            try
            {
                var xWell = WellPlateWellPositionTranslator.GetWellPosition(x.WellLocation);
                var yWell = WellPlateWellPositionTranslator.GetWellPosition(y.WellLocation);

                if (Regex.IsMatch(xWell, @",\d{1}$"))
                {
                    string number = Regex.Match(xWell, @"\d{1}$").Value;
                    xWell = Regex.Replace(xWell, @",\d{1}$", $",{number}0");
                }

                if (Regex.IsMatch(yWell, @",\d{1}$"))
                {
                    string number = Regex.Match(yWell, @"\d{1}$").Value;
                    yWell = Regex.Replace(yWell, @",\d{1}$", $",{number}0");
                }

                var result = xWell.CompareTo(yWell);
                return result;
            }
            catch (Exception exception)
            {
                
            }

            return 0;
        }
    }
}
