﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class DataComparerByFirstBeforeAfterAndTaskName : IComparer<MassLynxSamplesFileWorklistSheetModel>
    {
        private const string BEFORE = "before";
        private const string AFTER = "after";

        public int Compare(MassLynxSamplesFileWorklistSheetModel x, MassLynxSamplesFileWorklistSheetModel y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }

            if (x.IntelliScanFileModel != null && y.IntelliScanFileModel != null)
            {
                //order by username
                if (String.IsNullOrWhiteSpace(x.IntelliScanFileModel.UserName) && String.IsNullOrWhiteSpace(y.IntelliScanFileModel.UserName))
                {
                    return 0;
                }
                if (String.IsNullOrWhiteSpace(x.IntelliScanFileModel.UserName) && !String.IsNullOrWhiteSpace(y.IntelliScanFileModel.UserName))
                {
                    return 1;
                }
                if (!String.IsNullOrWhiteSpace(x.IntelliScanFileModel.UserName) && String.IsNullOrWhiteSpace(y.IntelliScanFileModel.UserName))
                {
                    return -1;
                }
                int userNameResult = x.IntelliScanFileModel.UserName.CompareTo(y.IntelliScanFileModel.UserName);
                if (userNameResult != 0)
                {
                    return userNameResult;
                }

                //order by compound (task)
                if (String.IsNullOrWhiteSpace(x.IntelliScanFileModel.Compound) && String.IsNullOrWhiteSpace(y.IntelliScanFileModel.Compound))
                {
                    return 0;
                }
                if (String.IsNullOrWhiteSpace(x.IntelliScanFileModel.Compound) && !String.IsNullOrWhiteSpace(y.IntelliScanFileModel.Compound))
                {
                    return 1;
                }
                if (!String.IsNullOrWhiteSpace(x.IntelliScanFileModel.Compound) && String.IsNullOrWhiteSpace(y.IntelliScanFileModel.Compound))
                {
                    return -1;
                }
                int samplaIdResult = x.IntelliScanFileModel.Compound.CompareTo(y.IntelliScanFileModel.Compound);
                return samplaIdResult;
            }

            if (x.Standard != null && y.Standard != null)
            {
                return 0;
            }

            if (x.Standard == null && y.Standard != null)
            {
                if (y.Standard.Notes.ToLower().StartsWith(BEFORE))
                {
                    return 1;
                }
                if (y.Standard.Notes.ToLower().StartsWith(AFTER))
                {
                    return -1;
                }
            }
            if (x.Standard != null && y.Standard == null)
            {
                if (x.Standard.Notes.ToLower().StartsWith(BEFORE))
                {
                    return -1;
                }
                if (x.Standard.Notes.ToLower().StartsWith(AFTER))
                {
                    return 1;
                }
            }

            return 0;
        }
    }
}
