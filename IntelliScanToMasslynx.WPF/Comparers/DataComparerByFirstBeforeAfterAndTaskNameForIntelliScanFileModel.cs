﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class DataComparerByFirstBeforeAfterAndTaskNameForIntelliScanFileModel : IComparer<IntelliScanFileModel>
    {
        private readonly List<string> INLET_ORDER = new List<string> { "HSA", "LogD", "IAM" };

        public int Compare(IntelliScanFileModel x, IntelliScanFileModel y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }

            //Order by username
            if (String.IsNullOrWhiteSpace(x.UserName) && String.IsNullOrWhiteSpace(y.UserName))
            {
                return 0;
            }
            if (String.IsNullOrWhiteSpace(x.UserName) && !String.IsNullOrWhiteSpace(y.UserName))
            {
                return 1;
            }
            if (!String.IsNullOrWhiteSpace(x.UserName) && String.IsNullOrWhiteSpace(y.UserName))
            {
                return -1;
            }
            int userNameResult = x.UserName.CompareTo(y.UserName);
            if (userNameResult != 0)
            {
                return userNameResult;
            }

            //Orde by Compound (SampleID)
            if (String.IsNullOrWhiteSpace(x.Compound) && String.IsNullOrWhiteSpace(y.Compound))
            {
                return 0;
            }
            if (String.IsNullOrWhiteSpace(x.Compound) && !String.IsNullOrWhiteSpace(y.Compound))
            {
                return 1;
            }
            if (!String.IsNullOrWhiteSpace(x.Compound) && String.IsNullOrWhiteSpace(y.Compound))
            {
                return -1;
            }
            int compoundResultResult = x.Compound.CompareTo(y.Compound);
            if (compoundResultResult != 0)
            {
                return compoundResultResult;
            }

            //Order by protocol name
            if (String.IsNullOrWhiteSpace(x.ProtocolName) && String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return 0;
            }
            if (String.IsNullOrWhiteSpace(x.ProtocolName) && !String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return 1;
            }
            if (!String.IsNullOrWhiteSpace(x.ProtocolName) && String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return -1;
            }
            int protocolNameResult = x.ProtocolName.CompareTo(y.ProtocolName);
            return protocolNameResult;
        }
    }
}
