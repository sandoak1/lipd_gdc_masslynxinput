﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF.Comparers
{
    public class DataComparerByFirstInletFileForIntelliScanFileModel : IComparer<IntelliScanFileModel>
    {
        private readonly List<string> INLET_ORDER = new List<string> { "HSA", "LogD", "IAM" };

        public int Compare(IntelliScanFileModel x, IntelliScanFileModel y)
        {
            if (x == null && y == null)
            {
                return 0;
            }
            if (x == null && y != null)
            {
                return 1;
            }
            if (x != null && y == null)
            {
                return -1;
            }

            if (String.IsNullOrWhiteSpace(x.ProtocolName) && String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return 0;
            }
            if (String.IsNullOrWhiteSpace(x.ProtocolName) && !String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return 1;
            }
            if (!String.IsNullOrWhiteSpace(x.ProtocolName) && String.IsNullOrWhiteSpace(y.ProtocolName))
            {
                return -1;
            }

            int indexX = INLET_ORDER.IndexOf(INLET_ORDER.FirstOrDefault(xx => x.ProtocolName.ToLower().Contains(xx.ToLower())));
            int indexY = INLET_ORDER.IndexOf(INLET_ORDER.FirstOrDefault(xx => y.ProtocolName.ToLower().Contains(xx.ToLower())));
            if (indexX == -1 && indexY == -1)
            {
                return 0;
            }
            if (indexX == -1 && indexY > -1)
            {
                return 1;
            }
            if (indexX > -1 && indexY == -1)
            {
                return -1;
            }

            if (indexX == indexY)
            {
                return 0;
            }
            if (indexX < indexY)
            {
                return -1;
            }
            if (indexX > indexY)
            {
                return 1;
            }

            return 0;
        }
    }
}
