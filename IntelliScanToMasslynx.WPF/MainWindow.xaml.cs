﻿using IntelliScanToMasslynx.WPF.Models;
using IntelliScanToMasslynx.WPF.Properties;
using Novartis.Ph.Nibr.Pla.Common;
using Novartis.Ph.Nibr.Pla.WPFUtilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace IntelliScanToMasslynx.WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        public MainWindow()
        {
            InitializeComponent();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            //test
            //Utilities.SetIsProdExternally = () => false;

            Controller.Instance.MainApplicationWindow = this;
        }

        private void InputIntelliScanBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.Instance.BrowseIntelliScanFile();
        }

        private void InputConfigurationBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.Instance.BrowseConfigurationFile();
        }

        private void InputPhysChemBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.Instance.BrowsePhyChemFile();
        }

        private void OutputFolderBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.Instance.BrowseOutputFolder();
        }

        private void btnConfigView_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!DataModel.Instance.IsConfigurationLoaded)
                {
                    ConfigurationFileReader configurationFileReader = new ConfigurationFileReader(DataModel.Instance.ConfigurationFilePath, this._cancellationTokenSource);
                    ConfigurationFileModel configuration = configurationFileReader.ReadData();

                    DataModel.Instance.Configuration = configuration;
                    DataModel.Instance.ConfigurationEdited = DataModel.Instance.Configuration.Clone() as ConfigurationFileModel;
                    DataModel.Instance.IsConfigurationLoaded = true;
                }
                ConfigurationEditWindow configurationEditWindow = new ConfigurationEditWindow(false);
                configurationEditWindow.Owner = this;
                configurationEditWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                configurationEditWindow.ShowDialog();
            }
            catch (Exception exception)
            {
                string message = "Error occured during showing the config.";
                Utilities.Instance.Log(LogLevels.Warning, message, exception);
                MessageBox.Show($"{message}{Environment.NewLine}{exception.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnConfigEdit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DataModel.Instance.IsConfigurationLoaded)
                {
                    DataModel.Instance.ConfigurationEdited = DataModel.Instance.Configuration.Clone() as ConfigurationFileModel;
                }
                else
                {
                    ConfigurationFileReader configurationFileReader = new ConfigurationFileReader(DataModel.Instance.ConfigurationFilePath, this._cancellationTokenSource);
                    ConfigurationFileModel configuration = configurationFileReader.ReadData();

                    DataModel.Instance.Configuration = configuration;
                    DataModel.Instance.ConfigurationEdited = DataModel.Instance.Configuration.Clone() as ConfigurationFileModel;
                    DataModel.Instance.IsConfigurationLoaded = true;
                }

                ConfigurationEditWindow configurationEditWindow = new ConfigurationEditWindow(true);
                configurationEditWindow.Owner = this;
                configurationEditWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                configurationEditWindow.ShowDialog();
                if (configurationEditWindow.WindowResult == WindowResult.Ok)
                {
                    DataModel.Instance.IsConfigurationEditedByTheUser = true;
                    DataModel.Instance.RunCountAfterUserEditedTheConfiguration = 0;
                }
            }
            catch (Exception exception)
            {
                string message = "Error occured during showing the config.";
                Utilities.Instance.Log(LogLevels.Warning, message, exception);
                MessageBox.Show($"{message}{Environment.NewLine}{exception.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            Controller.Instance.Execute();
        }

        private void OpenResultButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(DataModel.Instance.OutputFolderPath);
            }
            catch (Exception exception)
            {
                Utilities.Instance.Log(LogLevels.Error, $"Opening the output folder was not successfull.", exception);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Controller.Instance.CancellationTokenSource.IsCancellationRequested)
            {
                UIUtilities.AppendMessageToGUI("Cancelling...", LogLevels.Information, true, false);
                Controller.Instance.CancellationTokenSource.Cancel();
            }
        }

        private void AboutIconButton_Click(object sender, RoutedEventArgs e)
        {
            new About() { Owner = this }.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoginWindow loginWindow = new LoginWindow();
            loginWindow.Owner = this;
            loginWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            loginWindow.ShowDialog();
            if (loginWindow.LoginResult != LoginResult.Success)
            {
                if (loginWindow.LoginResult == LoginResult.Fialure)
                {
                    MessageBox.Show("Invalid username or password.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (loginWindow.LoginResult == LoginResult.Cancel)
                {
                    MessageBox.Show("You have to login if you would like to use this application.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                this.Close();
            }

            DataModel.Instance.UserName = loginWindow.UserName;

            if (!String.IsNullOrWhiteSpace(Settings.Default.LastPathIntelliScanFile) && File.Exists(Settings.Default.LastPathIntelliScanFile))
            {
                DataModel.Instance.IntelliScanFilePath = Settings.Default.LastPathIntelliScanFile;
            }
            if (!String.IsNullOrWhiteSpace(Settings.Default.LastPathConfigurationFile) && File.Exists(Settings.Default.LastPathConfigurationFile))
            {
                DataModel.Instance.ConfigurationFilePath = Settings.Default.LastPathConfigurationFile;
            }
            if (!String.IsNullOrWhiteSpace(Settings.Default.LastPathPhysChemFile) && File.Exists(Settings.Default.LastPathPhysChemFile))
            {
                DataModel.Instance.PhyChemFilePath = Settings.Default.LastPathPhysChemFile;
            }
            if (!String.IsNullOrWhiteSpace(Settings.Default.LastPathOutputFolder) && Directory.Exists(Settings.Default.LastPathOutputFolder))
            {
                DataModel.Instance.UserSelectedOutputFolderPath = Settings.Default.LastPathOutputFolder;
            }

            this.ValidateUiInputs();
        }

        private void ValidateUiInputs()
        {
            if (String.IsNullOrWhiteSpace(txtIntelliScanFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            //if (!txtIntelliScanFile.Text.ToLower().EndsWith(".xlsx"))
            //{
            //    DataModel.Instance.IsStartButtonEnabled = false;
            //    return;
            //}
            if (!txtIntelliScanFile.Text.ToLower().EndsWith(".xml"))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!File.Exists(txtIntelliScanFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(txtConfigurationFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!txtConfigurationFile.Text.ToLower().EndsWith(".xlsx"))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!File.Exists(txtConfigurationFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(txtPhyChemFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!txtPhyChemFile.Text.ToLower().EndsWith(".xlsx"))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!File.Exists(txtPhyChemFile.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }

            if (String.IsNullOrWhiteSpace(txtOutputFolder.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }
            if (!Directory.Exists(txtOutputFolder.Text))
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }


            if (String.IsNullOrWhiteSpace(txtFirstSample.Text))
            {
                
            }
            else
            {
                int parsed = -1;
                if (Int32.TryParse(txtFirstSample.Text, out parsed))
                {
                    if (parsed < 1)
                    {
                        DataModel.Instance.IsStartButtonEnabled = false;
                        return;
                    }
                }
                else
                {
                    DataModel.Instance.IsStartButtonEnabled = false;
                    return;
                }
            }

            if (DataModel.Instance.SelectedPlateNumber == null)
            {
                DataModel.Instance.IsStartButtonEnabled = false;
                return;
            }

            DataModel.Instance.IsStartButtonEnabled = true;
        }

        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.ValidateUiInputs();
        }

        private void cbPlateNumber_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.ValidateUiInputs();
        }
    }
}
