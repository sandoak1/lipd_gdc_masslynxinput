﻿using IntelliScanToMasslynx.WPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace IntelliScanToMasslynx.WPF
{
    /// <summary>
    /// Interaction logic for ConfigurationEditWindow.xaml
    /// </summary>
    public partial class ConfigurationEditWindow : Window
    {
        public WindowResult WindowResult { get; set; } = WindowResult.None;

        public ConfigurationEditWindow(bool isEnabledUI)
        {
            InitializeComponent();

            if (!isEnabledUI)
            {
                this.dataGridParameters.IsEnabled = false;
                this.dataGridStandards.IsEnabled = false;
                this.dataGridVolume.IsEnabled = false;
                this.OkButton.IsEnabled = false;
            }
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowResult = WindowResult.Ok;
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowResult = WindowResult.Cancel;
            this.Close();
        }
    }
}
