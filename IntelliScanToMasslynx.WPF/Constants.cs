﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntelliScanToMasslynx.WPF
{
    public static class Constants
    {
        public const string CONFIGURATION_PARAMETERS_SHEET_NAME = "Parameters";
        public const string CONFIGURATION_STANDARD_SHEET_NAME = "Standard";
        public const string CONFIGURATION_VOLUME_SHEET_NAME = "Volume";

        public const string PROTOCOL_NAME_FOR_ALL = "Trend Analysis All";

        public const string SubmittedSamplesSheetName = "Submitted Samples";
        public const string HSASheetName = "HSA";
        public const string HSACurveSheetName = "HSA curve";
        public const string LogDSheetName = "log D";
        public const string LogDCurveSheetName = "log D curve";
        public const string IAMSheetName = "IAM";
        public const string IAMCurveSheetName = "IAM curve";
        public const string VOfDistributionSheetName = "V of Distribution";
        public const string PharonUploadSheetName = "Pharon upload";
        public const string ResultsforchemistsSheetName = "Results for chemists";
    }
}
